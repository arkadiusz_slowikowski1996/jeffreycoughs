﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagecouchHorse : MonoBehaviour
{
    [SerializeField] private float speed;

    private new Rigidbody rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Vector3 _currPos = rigidbody.position;
        Vector3 _delta = Vector3.zero;
        _delta += transform.forward * Input.Axs("Vertical") * speed;
        _delta += transform.right * Input.Axs("Horizontal")* speed;
        _delta *= Time.deltaTime;
        rigidbody.MovePosition(_currPos + _delta);
    }
}