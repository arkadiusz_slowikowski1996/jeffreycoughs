﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : UIAboveHead
{
    [SerializeField] private Image barImage;
    [SerializeField] private Image backgroundImage;
    [SerializeField] private float width = 0.4f;

	protected override Graphic[] Graphics { get { return new Graphic[] { barImage, backgroundImage }; } }

	private void LateUpdate()
	{
		LookAtTarget();
	}

	public void SetValuesInit(float hpProc_, Transform toFollow_)
    {
        Rect _rect = barImage.rectTransform.rect;
        _rect.width = width * hpProc_;
        toFollow = toFollow_;
	}

    public void Set(float hpProc_)
    {
        Vector2 _rect = barImage.rectTransform.sizeDelta;
        _rect.x = width * hpProc_;
        barImage.rectTransform.sizeDelta = _rect;

        if (timeBeforeFading != 0 && timeOfFading != 0)
        {
            if (fadeAwayCoroutine != null) StopCoroutine(fadeAwayCoroutine);
            fadeAwayCoroutine = StartCoroutine(FadeAway());
        }
    }
}