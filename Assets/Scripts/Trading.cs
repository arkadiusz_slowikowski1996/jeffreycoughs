﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trading : MonoBehaviour
{
	public delegate void TradingEvent(string collectableName_, int amount_, string toWho_);
	public static event TradingEvent Traded;

	private void Update()
	{
		if (Input.BtnDwn("Trade", GameMode.GAME_STATE.GAMEPLAY))
		{
			WorldObject worldObject = Raycasting.hitWorldObject;

			if (Raycasting.IsHitInRange && worldObject)
			{
				if (worldObject.trader)
				{
					MainUI.StartTrading(worldObject.trader);
				}
			}
		}
	}

	public void FireTraded(string what_, int amount_, string toWho_)
	{
		Traded?.Invoke(what_, amount_, toWho_);
	}
}
