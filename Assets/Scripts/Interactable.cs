﻿using UnityEngine;

[RequireComponent(typeof(WorldObject))]
public class Interactable : WorldObjectComponent
{
    public enum INTERACTION_TYPE { POKE, INTERACT, PICKUP, COLLECT }
    [ReadOnly] public INTERACTION_TYPE interactionType;

    public virtual bool Use()
    {
        return true;
    }
}