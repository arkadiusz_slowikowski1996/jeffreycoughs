﻿using UnityEngine;
using UnityEngine.UI;

public class InventorySlotUI : MonoBehaviour
{
    public Button button;
    public Collectable collectable;

    [SerializeField] private Image collectableImage;
    [SerializeField] private Text stackAmountText;
    [SerializeField] private Color selectedColor;
    [SerializeField] private Color coveredColor;
    [SerializeField] private Color emptyColor;

    private Color defaultColor;
    private Sprite defaultSprite;
    private ItemDialogMenuUI.DIALOG_OPTIONS[] optionsAfterClick;

    private void Start()
    {
        defaultSprite = collectableImage.sprite;
        defaultColor = button.image.color;
    }

    public void Select()
    {
        ChangeColor(selectedColor);
    }

    public void Unselect()
    {
        ResetColor();
    }

    public void Cover()
    {
        ChangeColor(coveredColor);
    }

    public void Uncover()
    {
        ResetColor();
    }

    public void ItemClicked(InventorySlotUI button_)
    {
        if (button_.collectable)
        {
            ItemDialogMenuUI.Show(button_, MainUI.currWindow, optionsAfterClick);
        }
    }

    public void PlaceCollectable(Collectable collectable_, params ItemDialogMenuUI.DIALOG_OPTIONS[] options_)
    {
        optionsAfterClick = collectable_ ? options_ : new ItemDialogMenuUI.DIALOG_OPTIONS[0];
        button.interactable = collectable_;
        collectable = collectable_;

        Color _colorToSet;
        Sprite _spriteToSet;

        if (collectable_)
        {
            _spriteToSet = collectable_.imageInInventory;
            _colorToSet = new Color(1, 1, 1, 1);
            stackAmountText.transform.parent.gameObject.SetActive(true);
            stackAmountText.text = collectable_.currStackSize.ToString();
        }
        else
        {
            _spriteToSet = defaultSprite;
            _colorToSet = new Color(1, 1, 1, 0);
            stackAmountText.transform.parent.gameObject.SetActive(false);
        }

        collectableImage.sprite = _spriteToSet;
        collectableImage.color = _colorToSet;
    }

    public void RemoveCollectable()
    {
        collectable = null;
        collectableImage.sprite = null;
    }

    private void ChangeColor(Color newColor_)
    {
        button.image.color = newColor_;
    }

    private void ResetColor()
    {
        button.image.color = defaultColor;
    }
}