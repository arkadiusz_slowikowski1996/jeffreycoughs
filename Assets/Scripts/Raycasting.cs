﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycasting : MonoBehaviour
{
    public static RaycastHit currentHit;
    public static WorldObject hitWorldObject;
    private static Raycasting instance;
    private static float range = 1;

    [SerializeField] private bool raycastConstantly;
    [SerializeField] private Material outlineMaterial;

    private int layerMask;

    public static Vector3 Origin { get { return Camera.main.transform.position; } }

    public static bool IsHitInRange
    {
        get { return currentHit.collider ? Vector3.Distance(currentHit.point, InteractionReference.Position) < range : false; }
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        layerMask = ~(1 << LayerMask.NameToLayer("Player") << LayerMask.NameToLayer("Player_InnerCol") << LayerMask.NameToLayer("FeetCol"));
    }

    private void Update()
    {
        if (raycastConstantly && GameMode.currState == GameMode.GAME_STATE.GAMEPLAY)
        {
            RaycastFromCamera(out currentHit);
            SetHitWorldObject();
        }
    }

    public static bool RaycastFromCamera(out RaycastHit hit_, float maxOffset_ = 0)
    {
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * 20, Color.red);

        Vector3 origin = Camera.main.transform.position;// + 0.05f * Camera.main.transform.forward;
        Vector3 direction = Camera.main.transform.forward + Camera.main.transform.TransformDirection(new Vector3(Random.Range(-maxOffset_, maxOffset_), Random.Range(-maxOffset_, maxOffset_), Random.Range(-maxOffset_, maxOffset_)));
        return Physics.Raycast(origin, direction, out hit_, Mathf.Infinity, instance.layerMask);
    }

    public static bool RaycastFromCamera(out RaycastHit hit_, Vector3 disturbtion_)
    {
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * 20, Color.red);

        Vector3 origin = Camera.main.transform.position;// + 0.05f * Camera.main.transform.forward;
        Vector3 direction = Camera.main.transform.forward + disturbtion_;
        return Physics.Raycast(origin, direction, out hit_, Mathf.Infinity, instance.layerMask);
    }

    private void SetHitWorldObject()
    {
        WorldObject curr = currentHit.collider ? currentHit.collider.GetComponentInParent<WorldObject>() : null;

        if (curr != hitWorldObject)
        {
            if (curr)
            {
                HandleOutlineMaterial(true, curr.renderers);
            }
            else
            {
                HandleOutlineMaterial(false, hitWorldObject.renderers);
            }

            hitWorldObject = curr;
        }
    }

    private void HandleOutlineMaterial(bool add, Renderer[] renderers)
    {
        //foreach (Renderer renderer in renderers)
        //{
        //    List<Material> materials = new List<Material>();

        //    foreach (Material m in renderer.materials)
        //    {
        //        materials.Add(m);
        //    }

        //    if (add)
        //        materials.Add(outlineMaterial);
        //    else
        //        materials.RemoveAt(materials.Count - 1);

        //    renderer.materials = materials.ToArray();
        //}
    }
}