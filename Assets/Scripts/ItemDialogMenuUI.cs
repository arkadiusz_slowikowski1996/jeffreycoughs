﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDialogMenuUI : MonoBehaviour
{
	private static ItemDialogMenuUI ciop;

	public enum DIALOG_OPTIONS { DROP, CARRY, EQUIP, SELL, BUY, UNEQUIP, USE }

	[SerializeField] private Vector3 offset;
	[SerializeField] private Button option_prefab;
	[SerializeField] private Transform options_transform;

	private InventorySlotUI currentTarget;
	private WindowUI openedFrom;
	private List<Button> currentOptions = new List<Button>();

	public static bool IsOpen
	{
		get { return ciop ? ciop.gameObject.activeInHierarchy : false; }
	}

	private void Awake()
	{
		if (ciop) Debug.LogError("CWEL");

		ciop = this;
		gameObject.SetActive(false);
	}

	private void Update()
	{
		if (Input.BtnDwn("CancelUI", GameMode.GAME_STATE.UI) && IsOpen)
			Close(true);
	}

	public static void Show(InventorySlotUI target, WindowUI openedFrom, params DIALOG_OPTIONS[] dialogOptions)
	{
		if (IsOpen)
		{
			if (ciop.currentTarget != target)
				ciop.Close(false);
			else
				return;
		}

		ciop.currentTarget = target;
		ciop.openedFrom = openedFrom;
		ciop.transform.position = target.button.transform.position + ciop.offset;

		for (int i = 0; i < dialogOptions.Length; i++)
		{
			Button option = Instantiate(ciop.option_prefab, ciop.options_transform);
			option.GetComponentInChildren<Text>().text = GetOptionText(dialogOptions[i]);
			option.transform.localPosition = option.image.rectTransform.sizeDelta.y * -(ciop.currentOptions.Count + 1) * Vector3.up;
			//option.onClick.AddListener(delegate { DialogOptionClicked(dialogOptions[i]); });

			switch (i)
			{
				case 0:
					option.onClick.AddListener(delegate { DialogOptionClicked(dialogOptions[0]); });
					break;
				case 1:
					option.onClick.AddListener(delegate { DialogOptionClicked(dialogOptions[1]); });
					break;
				case 2:
					option.onClick.AddListener(delegate { DialogOptionClicked(dialogOptions[2]); });
					break;
				case 3:
					option.onClick.AddListener(delegate { DialogOptionClicked(dialogOptions[3]); });
					break;
				case 4:
					option.onClick.AddListener(delegate { DialogOptionClicked(dialogOptions[4]); });
					break;
				case 5:
					option.onClick.AddListener(delegate { DialogOptionClicked(dialogOptions[5]); });
					break;
			}

			ciop.currentOptions.Add(option);

			if (i != 0)
			{
				Navigation navigation = option.navigation;
				navigation.selectOnUp = ciop.currentOptions[i - 1];
				option.navigation = navigation;

				navigation = ciop.currentOptions[i - 1].navigation;
				navigation.selectOnDown = option;
				ciop.currentOptions[i - 1].navigation = navigation;
			}
		}

		ciop.gameObject.SetActive(true);
	}

	public void Close(bool withFocus_)
	{
		foreach (Button b in currentOptions)
			Destroy(b.gameObject);
		currentOptions.Clear();
		AmountUI.Close(false);
		ciop.gameObject.SetActive(false);

		if (withFocus_) openedFrom.Focus();
	}

	public static void DialogOptionClicked(DIALOG_OPTIONS dialogOption_)
	{
		ciop.StartCoroutine(ciop.DialogOptionClickedCor(dialogOption_));
	}

	private IEnumerator DialogOptionClickedCor(DIALOG_OPTIONS dialogOption_)
	{
		bool last;
		Collectable money, obtained;

		switch (dialogOption_)
		{
			case DIALOG_OPTIONS.BUY:
				yield return AmountUI.Show(currentTarget.collectable.currStackSize);
				if (AmountUI.successful)
				{
					money = GameMode.jeffrey.inventory.RemoveFromInventory("Coin", TradingUI.target.MoneyFor(currentTarget.collectable, true), out last);
					obtained = TradingUI.target.Exchange(currentTarget.collectable, AmountUI.currAmount, money);
					GameMode.jeffrey.inventory.AddToInventory(obtained);
				}
				break;
			case DIALOG_OPTIONS.CARRY:
				obtained = GameMode.jeffrey.inventory.RemoveFromInventory(currentTarget.collectable, 1, out last);
				openedFrom.GetComponent<InspectJeffreyUI>().RemoveFromInventory(obtained, true);
				break;
			case DIALOG_OPTIONS.DROP:
				yield return AmountUI.Show(currentTarget.collectable.currStackSize);
				if (AmountUI.successful)
				{
					obtained = GameMode.jeffrey.inventory.RemoveFromInventory(currentTarget.collectable, AmountUI.currAmount, out last);
					openedFrom.GetComponent<InspectJeffreyUI>().RemoveFromInventory(obtained, false);
				}
				break;
			case DIALOG_OPTIONS.EQUIP:
				yield return AmountUI.Show(currentTarget.collectable.currStackSize);
				yield return HandUI.Show();
				Equippable _equippable = GameMode.jeffrey.inventory.RemoveFromInventory(currentTarget.collectable, AmountUI.currAmount, out last) as Equippable;
				GameMode.jeffrey.equipping.Equip(_equippable, HandUI.currHand);

				break;
			case DIALOG_OPTIONS.SELL:
				yield return AmountUI.Show(currentTarget.collectable.currStackSize);
				if (AmountUI.successful)
				{
					Trader.REASONS _reason;
					bool _canBeTraded = TradingUI.target.CanBeExchanged("Coin", AmountUI.currAmount, currentTarget.collectable, out _reason);
					if (_canBeTraded)
					{
						money = TradingUI.target.inventory.RemoveFromInventory("Coin", TradingUI.target.MoneyFor(currentTarget.collectable, true), out last);
						obtained = TradingUI.target.Exchange(money, TradingUI.target.MoneyFor(currentTarget.collectable, true), currentTarget.collectable);
						GameMode.jeffrey.inventory.RemoveFromInventory(currentTarget.collectable, AmountUI.currAmount, out last);
						GameMode.jeffrey.inventory.AddToInventory(obtained);
					}
					else
					{
						Debug.Log(_reason.ToString());
					}
				}
				break;
			case DIALOG_OPTIONS.USE:

				break;
		}
		openedFrom.Refresh();
		Close(true);
	}

	public static string GetOptionText(DIALOG_OPTIONS dialogOption)
	{
		return dialogOption.ToString();
	}
}