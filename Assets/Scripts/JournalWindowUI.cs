﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class JournalWindowUI : MonoBehaviour
{
	public abstract void OnWindowOpened();
	public abstract void OnWindowClosed();
}