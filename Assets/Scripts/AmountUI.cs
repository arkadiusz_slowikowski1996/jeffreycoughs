﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using static GameMode;

public class AmountUI : MonoBehaviour
{
    private static AmountUI ciop;
    public static int currAmount;
    public static bool successful = false;

    [SerializeField] private Text amountText;
    [SerializeField] private Button body;

    private Coroutine coroutine;
    private float timeBeforeRepeat = 0.5f;
    private float repeatTime = 0.1f;
    private int iterations0 = 30;
    private int iterations1 = 100;
    private float mul0 = 0.2f;
    private float mul1 = 0.05f;

    private void Awake()
    {
        ciop = this;
    }

    public static IEnumerator Show(int maxAmount_)
    {
        successful = false;
        yield return ciop.ShowOnInstance(maxAmount_);
    }

    private IEnumerator ShowOnInstance(int maxAmount_)
    {
        currAmount = 0;
        ChangeValue(true, maxAmount_);

        body.Select();

        if (maxAmount_ == 1)
        {
            currAmount = 1;
            successful = true;
        }
        else
        {
            body.gameObject.SetActive(true);
            yield return coroutine = StartCoroutine(Counting(maxAmount_));
        }
    }

    public static void Close(bool successful)
    {
        ciop.body.gameObject.SetActive(false);
    }

    private IEnumerator Counting(int max_)
    {
        bool doLoop = true;

        while (doLoop)
        {
            if (Input.AxsRw("HorizontalUI", GAME_STATE.UI) != 0)
            {
                for (int i = -1; i <= 1; i++)
                {
                    if (i == 0) continue;

                    if (Input.AxsRw("HorizontalUI", GAME_STATE.UI) == i)
                    {
                        ChangeValue(i == 1, max_);

                        float _timer = 0;

                        while (Input.AxsRw("HorizontalUI", GAME_STATE.UI) == i)
                        {
                            _timer += Time.deltaTime;

                            if (_timer > timeBeforeRepeat)
                            {
                                _timer = 0;

                                int _its = 0;

                                while (Input.AxsRw("HorizontalUI", GAME_STATE.UI) == i)
                                {
                                    _timer += Time.deltaTime;

                                    if (_timer > (_its < iterations0 ? 1 : (_its > iterations0 && _its < iterations1 ? mul0 : mul1)) * repeatTime)
                                    {
                                        _timer = 0;
                                        _its++;

                                        ChangeValue(i == 1, max_);
                                    }

                                    yield return 0;
                                }
                            }

                            yield return 0;
                        }
                    }
                }
            }

            yield return 0;

            if (Input.BtnDwn("SubmitUI", GAME_STATE.UI) || Input.BtnDwn("CancelUI", GAME_STATE.UI))
            {
                Close(Input.BtnDwn("CancelUI", GAME_STATE.UI));
                doLoop = false;
            }
        }
    }

    private void ChangeValue(bool increment_, int maxAmount_)
    {
        currAmount += increment_ ? 1 : -1;
        currAmount = Mathf.Clamp(currAmount, 1, maxAmount_);
        amountText.text = currAmount.ToString();
    }
}