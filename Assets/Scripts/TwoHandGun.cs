﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoHandGun : Gun
{
    [Header("--TWO HAND GUN--")]
    [SerializeField] private int numberOfBulletsAtShot;
    [SerializeField] private float buildInDisturbance;

    protected override bool Usage0()
    {
        return Fire();
    }

    protected override bool Usage1()
    {
        return Fire();
    }

    protected override void Shoot()
    {
        for (int i = 0; i < numberOfBulletsAtShot; i++)
        {
            Raycasting.RaycastFromCamera(out RaycastHit _hit, GameMode.jeffrey.disturbtionWhenMoving.GetDisturbionWithExtra(currRecoilTime / RecoilTime) + buildInDisturbance);

            if (_hit.collider && _hit.collider.attachedRigidbody)
            {
                Vector3 _dir = (_hit.point - Raycasting.Origin).normalized;
                _hit.collider.attachedRigidbody.AddForce(_dir * explosionForce);
            }

            //GameObject _shot = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //_shot.transform.localScale = Vector3.one * 0.2f;
            //_shot.transform.position = _hit.point;
        }

        if (addExtraDisturbtionCoroutine != null) StopCoroutine(addExtraDisturbtionCoroutine);
        addExtraDisturbtionCoroutine = StartCoroutine(AddExtraDisturbtion());
    }
}