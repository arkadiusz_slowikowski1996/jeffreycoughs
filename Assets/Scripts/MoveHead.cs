﻿using UnityEngine;

public class MoveHead : MonoBehaviour
{
	[SerializeField] private new Transform head;
	[SerializeField] private float verticalSpeed = 2.0F;

	private float EulerAngle_X { get { return head.eulerAngles.x > 180 ? -(360 - head.eulerAngles.x) : head.eulerAngles.x; } }

	public Vector3 _LocalEulerAngles;
	public Vector3 LocalEulerAngles
	{
		get
		{
			Vector3 returnValue;

			returnValue.x = head.localEulerAngles.x > 180 ? -(360 - head.localEulerAngles.x) : head.localEulerAngles.x;
			returnValue.y = head.localEulerAngles.y > 180 ? -(360 - head.localEulerAngles.y) : head.localEulerAngles.y;
			returnValue.z = head.localEulerAngles.z > 180 ? -(360 - head.localEulerAngles.z) : head.localEulerAngles.z;

			return returnValue;
		}
	}

	private void LateUpdate()
	{
		_LocalEulerAngles = LocalEulerAngles;

		FPSCamera();
	}

	private void FPSCamera()
	{
		float v = verticalSpeed * Input.Axs("Mouse Y", GameMode.GAME_STATE.GAMEPLAY) * Time.deltaTime;
		//head.eulerAngles += new Vector3(-v, 0, 0);
		head.eulerAngles = new Vector3(Mathf.Clamp(EulerAngle_X - v, -80, 80), head.eulerAngles.y, head.eulerAngles.z);
	}
}