﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TalkingSounds
{
	public enum SOUNDS { DEFAULT0, DEFAULT1 };

	private static AudioClip[] sounds;

	public static AudioClip GetSound(SOUNDS sound_)
	{
		if (sounds == null)
		{
			sounds = new AudioClip[1];
		}
		else if (sounds.Length <= (int)sound_)
		{
			AudioClip[] _temp = new AudioClip[(int)sound_ + 1];
			for (int i = 0; i < sounds.Length; i++)
				_temp[i] = sounds[i];
			sounds = _temp;
		}

		if (sounds[(int)sound_] == null)
			sounds[(int)sound_] = Resources.Load<AudioClip>("Talking Sounds/" + sound_.ToString());

		return sounds[(int)sound_];
	}
}