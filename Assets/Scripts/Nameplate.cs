﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Nameplate : UIAboveHead
{
	public Text nameText;
	public Text outlineText;
	public Outline outline;

	private Color originalOutlineColor;
	private Coroutine glowingCoroutine;

	protected override Graphic[] Graphics { get { return new Graphic[] { nameText, outlineText }; } }

	private void Start()
	{
		originalOutlineColor = outline.effectColor;
	}

	private void LateUpdate()
	{
		LookAtTarget();
	}

	public void SetValues(string name_, Transform toFollow_)
	{
		nameText.text = name_;
		outlineText.text = name_;
		toFollow = toFollow_;
	}

	public void GlowOutline(Color dest_, float interval_)
	{
		if (glowingCoroutine != null) StopCoroutine(glowingCoroutine);
		glowingCoroutine = StartCoroutine(Glow(dest_, interval_));
	}

	public void StopGlowOutline()
	{
		StopCoroutine(glowingCoroutine);
		glowingCoroutine = null;
		outline.effectColor = originalOutlineColor;
	}

	private IEnumerator Glow(Color dest_, float interval_)
	{
		bool _back = false;
		while (true)
		{
			Color _start = _back ? dest_ : originalOutlineColor;
			Color _final = _back ? originalOutlineColor : dest_;

			float _timer = 0;
			while (_timer < interval_)
			{
				_timer += Time.deltaTime;
				outline.effectColor = Color.Lerp(_start, _final, _timer / interval_);
				yield return 0;
			}

			_back = !_back;
		}
	}
}