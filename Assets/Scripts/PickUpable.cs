﻿using System;
using UnityEngine;

[Serializable]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(WorldObject))]
public class PickUpable : WorldObjectComponent
{
    public new Rigidbody rigidbody;

    private void Reset()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
}