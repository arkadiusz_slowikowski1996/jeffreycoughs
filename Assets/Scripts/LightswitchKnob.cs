﻿using UnityEngine;

public class LightswitchKnob : Interactable
{
    [SerializeField] private Light controlledLight;
    [SerializeField] private Vector3 enabled_eulerAngles;
    [SerializeField] private Vector3 disabled_eulerAngles;

    public override bool Use()
    {
        controlledLight.enabled = !controlledLight.enabled;
        transform.localEulerAngles = controlledLight.enabled ? enabled_eulerAngles : disabled_eulerAngles;
        return true;
    }
}