﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Health : WorldObjectComponent
{
    //public delegate void Dying(WorldObject target_);
    //public static Dying Died;
    //public delegate void TakingDamage(Health target_, int damageTaken_);
    //public static event TakingDamage DamageTaken;

    [SerializeField] private int maxHp = 100;
    [SerializeField] private int currHp = 100;
    [SerializeField] private bool showBar = true;

    [SerializeField] private Transform hpBarPos;
    [SerializeField] private HPBar hpBarPrefab;

    [SerializeField] private HPBar myHPBar;

    private float HPProc { get { return (float)(((float)currHp) / ((float)maxHp)); } }

    private void Start()
    {
        if (!myHPBar && showBar)
        {
            myHPBar = Instantiate(hpBarPrefab, GameMode.WorldCanvas.transform);
            myHPBar.SetValuesInit(HPProc, hpBarPos);
            myHPBar.transform.position = hpBarPos.position;
        }
    }

    public void TakeDamage(int dmg_)
    {
        Debug.Log("Damage taken: " + dmg_);
        currHp -= dmg_;

        if (currHp <= 0)
        {
            //Died?.Invoke(worldObject);
        }
        else
        {
            myHPBar.Set(HPProc);
        }
    }
}