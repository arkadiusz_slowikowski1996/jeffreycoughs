﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class Collectable : MonoBehaviour
{
    public enum CATEGORY { TRASH, GUN, MISC, FOOD, AMMO, MONEY }
    public CATEGORY category;

    [ReadOnly] public string colName;
    public new Rigidbody rigidbody;

    public int currStackSize = 1;
    public int maxStackSize = 999;
    public int sizeInInventory = 1;

    public int defaultValue;
    public Sprite imageInInventory;

    public bool Stackable
    { get { return maxStackSize == 1; } }

    private void Start()
    {
        name = colName;
    }

    private void OnValidate()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
}