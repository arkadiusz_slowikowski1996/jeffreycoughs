﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagecoachWheels : MonoBehaviour
{
	[SerializeField] private Transform[] meshes;
	[SerializeField] private WheelCollider[] colliders;

	private void Update()
	{
		for (int i = 0; i < meshes.Length; i++)
		{
			colliders[i].GetWorldPose(out Vector3 _pos, out Quaternion _rot);

			meshes[i].position = _pos;
			meshes[i].rotation = _rot;
		}
	}
}