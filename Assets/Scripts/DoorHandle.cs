﻿using UnityEngine;

public class DoorHandle : Interactable
{
    private static float doorDistanceToClose = 0.5f;

    [SerializeField] private Rigidbody door;
    [SerializeField] private Vector3 closedEulerAngles;
    [SerializeField] private float openDoorTorque;

    private bool locked;
    private bool doAutomaticallyClose;

    private bool Closed
    {
        get { return door.isKinematic; }
        set { door.isKinematic = value; }
    }

    private void Update()
    {
        if (doAutomaticallyClose && door.transform.eulerAngles != closedEulerAngles)
        {
            if (LocalEulerAngles(door.transform).y < doorDistanceToClose)
            {
                door.transform.localEulerAngles = closedEulerAngles;
                Closed = true;
            }
        }

        if (!doAutomaticallyClose && LocalEulerAngles(door.transform).y >= doorDistanceToClose * 2)
        {
            doAutomaticallyClose = true;
        }
    }

    public override bool Use()
    {
        if (!locked)
        {
            if (Closed)
            {
                Closed = false;
                doAutomaticallyClose = false;
                door.AddTorque(transform.parent.up * openDoorTorque);
            }
            //else
            //{
            //    doAutomaticallyClose = true;
            //    door.AddTorque(transform.parent.up * -2 * openDoorTorque);
            //}

            return true;
        }

        return false;
    }

    public Vector3 LocalEulerAngles(Transform transform)
    {
        Vector3 returnValue;

        returnValue.x = transform.localEulerAngles.x > 180 ? -(360 - transform.localEulerAngles.x) : transform.localEulerAngles.x;
        returnValue.y = transform.localEulerAngles.y > 180 ? -(360 - transform.localEulerAngles.y) : transform.localEulerAngles.y;
        returnValue.z = transform.localEulerAngles.z > 180 ? -(360 - transform.localEulerAngles.z) : transform.localEulerAngles.z;

        return returnValue;
    }
}