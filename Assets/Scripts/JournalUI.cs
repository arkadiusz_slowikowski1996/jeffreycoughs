﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalUI : WindowUI
{
    private static JournalUI instance;

    [SerializeField] private CardNameUI[] cardNames;

    private int currIndex;
    private int CurrIndex
    {
        get { return currIndex; }
        set
        {
            value %= cardNames.Length;
            if (value < 0) value = cardNames.Length - 1;
            cardNames[currIndex].SetActive(false);
            currIndex = value;
            cardNames[currIndex].SetActive(true);
        }
    }

    private float prevAxisRaw;

    public static Button DefaultFocusTarget { set { instance.defaultFocusTarget = value; } }

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        float axisRaw = Input.AxsRw("JournalSwitch", GameMode.GAME_STATE.UI);
        if (axisRaw != prevAxisRaw)
        {
            CurrIndex += Mathf.CeilToInt(axisRaw);
        }
        prevAxisRaw = axisRaw;
    }

    public static void FocusStatic()
    {
        instance.Focus();
    }

    protected override void OnWindowOpened()
    {
        CurrIndex = 1;
    }

    protected override void OnWindowClosed()
    {
    }
}