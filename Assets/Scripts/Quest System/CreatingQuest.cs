﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using static Line;
using static QuestCondition;
using static QuestObjective;
using static TalkingSounds;

public class CreatingQuest : EditorWindow
{
	//private enum OBJECTIVE_TYPE { KILL, TALK_TO, COLLECT, TRADE }
	//private Enum _typeSelected = OBJECTIVE_TYPE.KILL;

	[MenuItem("Assets/Create/Peon's/Quest")]
	public static QuestScriptable Create()
	{
		QuestScriptable asset = ScriptableObject.CreateInstance<QuestScriptable>();

		AssetDatabase.CreateAsset(asset, "Assets/Quests/Quest.asset");
		AssetDatabase.SaveAssets();
		return asset;
	}
}

#endif