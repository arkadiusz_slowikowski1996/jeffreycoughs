﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Questing : MonoBehaviour
{
    public delegate void QuestingEvent(Quest quest_);
    public static event QuestingEvent QuestAccepted;
    public static event QuestingEvent QuestStageCompleted;
    public static event QuestingEvent QuestLastStageCompleted;
    public static event QuestingEvent QuestCompleted;

    public static List<Quest> currentQuests = new List<Quest>();
    public static List<Quest> finishedQuests = new List<Quest>();

    public static void TakeQuest(Quest quest_)
    {
        if (currentQuests.GetQuest(quest_.questTitle) != null)
        {
            Debug.LogError("ZJ3B");
        }
        else
        {
            currentQuests.Add(quest_);
            QuestAccepted?.Invoke(quest_);
            quest_.Activate();
        }
    }

    public static void LastStageCompleted(Quest quest_)
    {
        QuestLastStageCompleted?.Invoke(quest_);
    }

    public static void CompleteQuest(Quest quest_)
    {
        bool _succ = currentQuests.Remove(quest_);

        if (!_succ) Debug.LogError("zjeeb");

        Debug.Log("Quest completed");

        finishedQuests.Add(quest_);
        QuestCompleted?.Invoke(quest_);
        quest_.Complete();
    }
}