﻿using System;

[Serializable]
public class QuestStage
{
	public QuestObjective[] objectives;
	public string description;

	public static QuestStage Copy(QuestStage original_)
	{
		QuestObjective[] _newQuestObjectives = new QuestObjective[original_.objectives.Length];
		for (int i = 0; i < _newQuestObjectives.Length; i++)
			_newQuestObjectives[i] = QuestObjective.Copy(original_.objectives[i]);

		return new QuestStage()
		{
			objectives = _newQuestObjectives,
			description = original_.description
		};
	}

	public static QuestStage[] Copy(QuestStage[] original_)
	{
		QuestStage[] _returnValue = new QuestStage[original_.Length];

		for (int i = 0; i < original_.Length; i++)
			_returnValue[i] = Copy(original_[i]);

		return _returnValue;
	}
}