﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiver : WorldObjectComponent
{
	private static float glowingNamespaceInternval = 0.5f;

	public string qg_name;

	public List<Quest> questsReadyToBeTaken = new List<Quest>();
	public List<Quest> questsReadyToBeCollected = new List<Quest>();

	[SerializeField] private QuestScriptable[] allQuestsICanGive;

	private void Start()
	{
		Questing.QuestAccepted += Questing_QuestAccepted;
		Questing.QuestCompleted += Questing_QuestCompleted;
		Questing.QuestLastStageCompleted += Questing_QuestLastStageCompleted;
	}

	public void UpdateInfo()
	{
		if (questsReadyToBeCollected.Count > 0)
		{
			worldObject.showingNameplate.nameplate.outline.effectColor = Color.green;
			worldObject.showingNameplate.nameplate.GlowOutline(Color.green, glowingNamespaceInternval);
		}
		else if (questsReadyToBeTaken.Count > 0)
		{
			worldObject.showingNameplate.nameplate.outline.effectColor = Color.yellow;
			worldObject.showingNameplate.nameplate.GlowOutline(Color.yellow, glowingNamespaceInternval);
		}
		else
		{
			worldObject.showingNameplate.nameplate.outline.effectColor = Color.white;
			worldObject.showingNameplate.nameplate.StopGlowOutline();
		}
	}

	public void AddTakableQuests()
	{
		foreach (QuestScriptable qs in allQuestsICanGive)
		{
			if (qs.IsTakable)
				questsReadyToBeTaken.Add(Quest.CopyFromScriptable(qs));
		}

		UpdateInfo();
	}

	private void Questing_QuestAccepted(Quest quest_)
	{
		Quest _entryInList = questsReadyToBeTaken.GetQuest(quest_.questTitle);

		if (_entryInList != null)
		{
			questsReadyToBeTaken.Remove(_entryInList);
			AddTakableQuests();
		}
	}

	private void Questing_QuestLastStageCompleted(Quest quest_)
	{
		if (quest_.returnTo != qg_name) return;

		if (questsReadyToBeCollected.Contains(quest_)) Debug.LogError("ZJEB");

		questsReadyToBeCollected.Add(quest_);

		UpdateInfo();
	}

	private void Questing_QuestCompleted(Quest quest_)
	{
		if (questsReadyToBeCollected.Contains(quest_))
		{
			questsReadyToBeCollected.Remove(quest_);

			UpdateInfo();
		}
	}
}