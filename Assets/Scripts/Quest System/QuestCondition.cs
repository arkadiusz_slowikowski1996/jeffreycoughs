﻿using System;

[Serializable]
public class QuestCondition
{
	public enum CONDITION_TYPE { KILLED, HAVE_IN_BACKPACK, FINISHED }
	public CONDITION_TYPE conditionType;

	public bool completed;
	public string target;

	public static QuestCondition Copy(QuestCondition original_)
	{
		return new QuestCondition()
		{
			conditionType = original_.conditionType,
			completed = original_.completed,
			target = original_.target
		};
	}

	public static QuestCondition[] Copy(QuestCondition[] original_)
	{
		QuestCondition[] _returnValue = new QuestCondition[original_.Length];

		for (int i = 0; i < original_.Length; i++)
		{
			_returnValue[i] = Copy(original_[i]);
		}

		return _returnValue;
	}
}