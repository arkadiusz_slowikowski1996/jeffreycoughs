﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest", menuName = "Peon's/Quest", order = 1)]
public class QuestScriptable : ScriptableObject
{
	public string questTitle;
	public string questGiver;
	public bool mainQuest;
	public QuestCondition[] conditions;
	public string chainName;
	public int numberInChain;
	public QuestStage[] stages;
	public QuestReward[] rewards;
	public string returnTo;

	public Line[] takingQuestLines;
	public Line[] doingQuestLines;
	public Line[] doneQuestLines;

	public bool IsTakable
	{
		get
		{
			foreach (QuestCondition qc in conditions)
				if (ExecuteCondition(qc) == false)
					return false;

			if (Questing.finishedQuests.GetQuest(questTitle) != null)
				return false;

			if (Questing.currentQuests.GetQuest(questTitle) != null)
				return false;

			return true;
		}
	}

	public static bool ExecuteCondition(QuestCondition questCondition_)
	{
		switch (questCondition_.conditionType)
		{
			case QuestCondition.CONDITION_TYPE.FINISHED:
				//Read from PlayerPrefs
				break;
			case QuestCondition.CONDITION_TYPE.HAVE_IN_BACKPACK:
				//Check backpack
				break;
			case QuestCondition.CONDITION_TYPE.KILLED:
				//Read from PlayerPrefs
				break;
		}

		return true;
	}
}