﻿using System;

[Serializable]
public class QuestObjective
{
	public enum OBJECTIVE_TYPE { KILL, TALK_TO, COLLECT, TRADE }
	public OBJECTIVE_TYPE objectiveType;

	public bool completed;
	public string target;
	public string target2;
	public int amount;

	public static QuestObjective Copy(QuestObjective original_)
	{
		return new QuestObjective()
		{
			objectiveType = original_.objectiveType,
			completed = original_.completed,
			target = original_.target,
			target2 = original_.target2,
			amount = original_.amount
		};
	}
}

//[Serializable]
//public class Kill_QO : QuestObjective
//{
//	public string target;
//	public int amount;
//}

//[Serializable]
//public class TalkTo_QO : QuestObjective
//{
//    public string who;
//}

//[Serializable]
//public class Collect_QO : QuestObjective
//{
//    public string what;
//    public int amount;
//}

//[Serializable]
//public class Trade_QO : QuestObjective
//{
//    public string what;
//    public string toWhom;
//    public int amount;
//}