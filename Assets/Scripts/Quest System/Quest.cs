﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Quest
{
	//public delegate void WorldObjectEvent(string target_);
	//public static event WorldObjectEvent TalkedTo;
	//public static event WorldObjectEvent Killed;
	//public delegate void CollectableEventV2(string collectableName_, int amount_, string target_);
	//public static event CollectableEventV2 Traded;

	public string questTitle;
	/// <summary>
	/// Name of the quest giver;
	/// </summary>
	public string questGiver;
	/// <summary>
	/// Is quest a part of one of the main plots?
	/// </summary>
	public bool mainQuest;
	/// <summary>
	/// Conditions to make the quest takable.
	/// </summary>
	public QuestCondition[] conditions;
	/// <summary>
	/// Part of what chain is this quest?
	/// </summary>
	public string chainName;
	/// <summary>
	/// Which quest in the chain is it? Two quest can't have the same number.
	/// </summary>
	public int numberInChain;
	/// <summary>
	/// Objectives to complete.
	/// </summary>
	public QuestStage[] stages;
	/// <summary>
	/// Rewards for the quest.
	/// </summary>
	public QuestReward[] rewards;
	/// <summary>
	/// Person to return the quest to.
	/// </summary>
	public string returnTo;

	public Line[] takingQuestLines;
	public Line[] doingQuestLines;
	public Line[] doneQuestLines;

	public int currentStage = -1;

	private Line[] AllLines
	{
		get
		{
			List<Line> _returnValue = new List<Line>();
			_returnValue.AddRange(takingQuestLines);
			_returnValue.AddRange(doingQuestLines);
			_returnValue.AddRange(doneQuestLines);
			return _returnValue.ToArray();
		}
	}

	public void Activate()
	{
		foreach (Line l in AllLines)
			l.author = questGiver;

		ActivateStage(++currentStage);

		Talking.Say(takingQuestLines);
	}

	public void Complete()
	{
		Talking.Say(doneQuestLines);
	}

	private void ActivateStage(int index_)
	{
		foreach (QuestObjective qo in stages[index_].objectives)
		{
			switch (qo.objectiveType)
			{
				case QuestObjective.OBJECTIVE_TYPE.KILL:
					//TODO: killing
					break;
				case QuestObjective.OBJECTIVE_TYPE.TALK_TO:
					Talking.TalkedTo += Talking_TalkedTo;
					break;
				case QuestObjective.OBJECTIVE_TYPE.COLLECT:
					Collecting.Collected += Collecting_Collected;
					break;
				case QuestObjective.OBJECTIVE_TYPE.TRADE:
					Trading.Traded += Trading_Traded;
					break;
			}
		}
	}

	private void Talking_TalkedTo(string target_)
	{
		foreach (QuestObjective qo in stages[currentStage].objectives)
		{
			if (!qo.completed && qo.objectiveType == QuestObjective.OBJECTIVE_TYPE.TALK_TO)
			{
				if (target_ == qo.target)
				{
					qo.completed = true;
					CheckAllObjectives();
				}
			}
		}
	}

	private void Collecting_Collected(string collectableName_, int amount_)
	{
		foreach (QuestObjective qo in stages[currentStage].objectives)
		{
			if (!qo.completed && qo.objectiveType == QuestObjective.OBJECTIVE_TYPE.COLLECT)
			{
				if (collectableName_ == qo.target)
				{
					qo.amount -= amount_;

					if (qo.amount <= 0)
					{
						qo.completed = true;
						CheckAllObjectives();
					}
				}
			}
		}
	}

	private void Trading_Traded(string collectableName_, int amount_, string toWho_)
	{
		foreach (QuestObjective qo in stages[currentStage].objectives)
		{
			if (!qo.completed && qo.objectiveType == QuestObjective.OBJECTIVE_TYPE.TRADE)
			{
				if (collectableName_ == qo.target && toWho_ == qo.target2)
				{
					qo.amount -= amount_;

					if (qo.amount <= 0)
					{
						qo.completed = true;
						CheckAllObjectives();
					}
				}
			}
		}
	}

	private void CheckAllObjectives()
	{
		foreach (QuestObjective qo in stages[currentStage].objectives)
			if (!qo.completed) return;

		CompleteStage();
	}

	private void CompleteStage()
	{
		Debug.Log("CompleteStage()");

		foreach (QuestObjective qo in stages[currentStage].objectives)
		{
			switch (qo.objectiveType)
			{
				case QuestObjective.OBJECTIVE_TYPE.KILL:
					//TODO: killing
					break;
				case QuestObjective.OBJECTIVE_TYPE.TALK_TO:
					Talking.TalkedTo -= Talking_TalkedTo;
					break;
				case QuestObjective.OBJECTIVE_TYPE.COLLECT:
					Collecting.Collected -= Collecting_Collected;
					break;
				case QuestObjective.OBJECTIVE_TYPE.TRADE:
					Trading.Traded -= Trading_Traded;
					break;
			}
		}

		++currentStage;
		if (stages.Length > currentStage)
		{
			Debug.Log("next stage");
			ActivateStage(currentStage);
		}
		else
		{
			Questing.LastStageCompleted(this);
			Debug.Log("quest ready to be completed rn");
		}
	}

	public bool ClaimReward()
	{
		if (GameMode.jeffrey.inventory.SpaceLeft < rewards.Length) return false;

		Collectable[] _rewards = new Collectable[rewards.Length];

		for (int i = 0; i < rewards.Length; i++)
		{
			_rewards[i] = GameObject.Instantiate(rewards[i].reward);
			_rewards[i].currStackSize = rewards[i].amount;

			if (_rewards[i].currStackSize > _rewards[i].maxStackSize) Debug.LogError("ZJEB");
		}

		for (int i = 0; i < _rewards.Length; i++)
		{
			GameMode.jeffrey.inventory.AddToInventory(_rewards[i]);
		}

		return true;
	}

	public static Quest CopyFromScriptable(QuestScriptable questScriptable_)
	{
		return new Quest()
		{
			questTitle = questScriptable_.questTitle,
			questGiver = questScriptable_.questGiver,
			mainQuest = questScriptable_.mainQuest,
			conditions = QuestCondition.Copy(questScriptable_.conditions),
			chainName = questScriptable_.chainName,
			numberInChain = questScriptable_.numberInChain,
			stages = QuestStage.Copy(questScriptable_.stages),
			rewards = QuestReward.Copy(questScriptable_.rewards),
			returnTo = questScriptable_.returnTo,
			takingQuestLines = new List<Line>(questScriptable_.takingQuestLines).ToArray(),
			doingQuestLines = new List<Line>(questScriptable_.doingQuestLines).ToArray(),
			doneQuestLines = new List<Line>(questScriptable_.doneQuestLines).ToArray()
		};
	}
}