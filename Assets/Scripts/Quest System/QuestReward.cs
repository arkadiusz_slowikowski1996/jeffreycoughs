﻿using System;

[Serializable]
public class QuestReward
{
    /// <summary>
    /// 'colName'
    /// </summary>
    public Collectable reward;
    public int amount;

	public static QuestReward Copy(QuestReward original_)
	{
		return new QuestReward()
		{
			reward = original_.reward,
			amount = original_.amount
		};
	}

	public static QuestReward[] Copy(QuestReward[] original_)
	{
		QuestReward[] _returnValue = new QuestReward[original_.Length];

		for (int i = 0; i < original_.Length; i++)
			_returnValue[i] = Copy(original_[i]);

		return _returnValue;
	}
}