﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WorldObject))]
public class Talker : WorldObjectComponent
{
    [SerializeField] private Line line;
    private QuestGiver questGiver;

    private void Start()
    {
        questGiver = GetComponent<QuestGiver>();
    }

    public void TalkTo()
    {
        Line _temp = Line.Copy(line, worldObject.name);
        Queue<Line> _linesSplit = new Queue<Line>();

        while (_temp.text.Length >= TalkingUI.textboxSize)
        {
            string _text = _temp.text.Substring(0, TalkingUI.textboxSize);
            _temp.text = _temp.text.Substring(TalkingUI.textboxSize);

            Line _newLine = new Line() { text = _text, displayMode = _temp.displayMode };
            _newLine.author = worldObject.objName;
            _linesSplit.Enqueue(_newLine);
        }

        if (questGiver)
        {
            foreach (Quest q in questGiver.questsReadyToBeTaken)
            {
                Decision_Talking _decision = Decision_Talking.CreateDecitionAboutQuest(q, Decision_Talking.DECISIONS.TAKE_A_QUEST);
                _temp.decisions.Add(_decision);
            }

            foreach (Quest q in questGiver.questsReadyToBeCollected)
            {
                Debug.Log("Quest ready to be taken");
                Decision_Talking _decision = Decision_Talking.CreateDecitionAboutQuest(q, Decision_Talking.DECISIONS.COLLECT_A_QUEST);
                _temp.decisions.Add(_decision);
            }
        }

        _linesSplit.Enqueue(_temp);

        while (_linesSplit.Count > 0)
            Talking.Say(_linesSplit.Dequeue());
    }
}