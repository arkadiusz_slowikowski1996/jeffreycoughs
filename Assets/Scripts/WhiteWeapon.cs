﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WhiteWeapon : Equippable
{
    [SerializeField] protected EquippablePose[] swingWaypoints;

    protected Coroutine swingCoroutine;

    public bool Swing()
    {
        if (swingCoroutine == null)
        {
            swingCoroutine = StartCoroutine(MoveOnTrack(swingWaypoints, () => { swingCoroutine = null; return 0; }));

            return true;
        }

        return false;
    }
}