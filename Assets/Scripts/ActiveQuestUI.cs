﻿using UnityEngine;
using UnityEngine.UI;

public class ActiveQuestUI : MonoBehaviour
{
	[SerializeField] private GameObject board;
	[SerializeField] private Text questTitle;
	[SerializeField] private Text stageDescription;

	private static ActiveQuestUI instance;

	private void Start()
	{
		instance = this;
	}

	public static void Show(Quest quest_)
	{
		instance.board.SetActive(quest_ != null);

		if (quest_ != null)
		{
			if (instance.questTitle.text == quest_.questTitle && instance.stageDescription.text == quest_.stages[quest_.currentStage].description)
			{
				instance.questTitle.text = "";
				instance.stageDescription.text = "";
			}
			else
			{
				instance.questTitle.text = quest_.questTitle;
				instance.stageDescription.text = quest_.stages[quest_.currentStage].description;
			}
		}
	}
}