﻿using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Talker))]
[RequireComponent(typeof(Trader))]
[RequireComponent(typeof(Interactable))]
public class WorldObject : MonoBehaviour
{
	private string GameObjectName { get { return base.name; } }

	public string objName = "-----";

	public Health health;
	public ShowingNameplate showingNameplate;
	public Interactable interactable;
	public PickUpable pickUpable;
	public Trader trader;
	public Talker talker;
	public Collectable collectable;
	public Renderer[] renderers;

	private void OnValidate()
	{
		if (collectable)
		{
			collectable.colName = objName;
		}
	}
}
#if UNITY_EDITOR
[CustomEditor(typeof(WorldObject))]
public class WorldObject_CustomEditor : Editor
{
	private new WorldObject target;

	private void OnEnable()
	{
		target = (WorldObject)base.target;
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (GUILayout.Button("Add Health"))
		{
			if (!target.GetComponent<Health>()) target.health = target.gameObject.AddComponent<Health>();
		}

		if (GUILayout.Button("Add ShowingNameplate"))
		{
			if (!target.GetComponent<ShowingNameplate>()) target.showingNameplate = target.gameObject.AddComponent<ShowingNameplate>();
		}

		if (GUILayout.Button("Add PickUpable"))
		{
			if (!target.GetComponent<PickUpable>()) target.pickUpable = target.gameObject.AddComponent<PickUpable>();
		}

		if (GUILayout.Button("Add Interactable"))
		{
			if (!target.GetComponent<Interactable>()) target.interactable = target.gameObject.AddComponent<Interactable>();
		}

		if (GUILayout.Button("Add Trader"))
		{
			if (!target.GetComponent<Trader>()) target.trader = target.gameObject.AddComponent<Trader>();
		}

		if (GUILayout.Button("Add Talker"))
		{
			if (!target.GetComponent<Talker>()) target.talker = target.gameObject.AddComponent<Talker>();
		}

		if (GUILayout.Button("Get Components"))
		{
			target.health = target.GetComponent<Health>();
			target.showingNameplate = target.GetComponent<ShowingNameplate>();
			target.pickUpable = target.GetComponent<PickUpable>();
			target.interactable = target.GetComponent<Interactable>();
			target.collectable = target.GetComponent<Collectable>();
			target.trader = target.GetComponent<Trader>();
			target.talker = target.GetComponent<Talker>();
			target.renderers = target.GetComponentsInChildren<Renderer>();
		}
	}
}
#endif