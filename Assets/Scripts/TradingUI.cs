﻿using UnityEngine;
using static ItemDialogMenuUI;

public class TradingUI : WindowUI
{
	public static Trader target;

	[SerializeField] private InventorySlotUI[] jeffreySlots;
	[SerializeField] private InventorySlotUI[] targetSlots;



	protected override void OnWindowOpened()
	{
		GameMode.currState = GameMode.GAME_STATE.UI;
		for (int i = 0; i < jeffreySlots.Length; i++)
		{
			jeffreySlots[i].PlaceCollectable(GameMode.jeffrey.inventory.content[i], DIALOG_OPTIONS.SELL);
		}

		for (int i = 0; i < targetSlots.Length; i++)
		{
			targetSlots[i].PlaceCollectable(target.inventory.content.Length > i ? target.inventory.content[i] : null, DIALOG_OPTIONS.BUY);
		}
	}

	protected override void OnWindowClosed()
	{

	}
}