﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardNameUI : MonoBehaviour
{
	public JournalWindowUI window;
	[SerializeField] private Text text;
	[SerializeField] private Outline outline;
	[SerializeField] private Color selectedColor;
	[SerializeField] private Color unselectedColor;

	public void SetActive(bool active_)
	{
		text.color = active_ ? selectedColor : unselectedColor;
		outline.enabled = active_;
		window.gameObject.SetActive(active_);
		if (window.gameObject.activeInHierarchy) window.OnWindowOpened();
		else window.OnWindowClosed();
	}
}