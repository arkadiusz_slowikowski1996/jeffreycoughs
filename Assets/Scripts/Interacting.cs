﻿using System;
using UnityEngine;

public class Interacting : MonoBehaviour
{
    private void Update()
    {
        if (Input.BtnDwn("Interact", GameMode.GAME_STATE.GAMEPLAY))
        {
            WorldObject worldObject = Raycasting.hitWorldObject;

            if (Raycasting.IsHitInRange && worldObject)
            {
                if (worldObject.interactable)
                {
                    worldObject.interactable.Use();
                }
            }
        }
    }
}