﻿using UnityEngine;
using static Equippable;

public class Equipping : MonoBehaviour
{
	[ReadOnly] public Equippable[] currentlyEquipped;

	[SerializeField] private Transform[] handTarget;
	[SerializeField] private Transform[] parent;

	private void Update()
	{

		if (Input.Btn("Reload", GameMode.GAME_STATE.GAMEPLAY))
		{
			for (int i = 0; i < handTarget.Length; i++)
			{
				if (Input.BtnDwn("Use" + i, GameMode.GAME_STATE.GAMEPLAY) && currentlyEquipped[i] && currentlyEquipped[i] is Gun)
					(currentlyEquipped[i] as Gun).Reload();
			}
		}
		else
		{
			for (int i = 0; i < handTarget.Length; i++)
			{
				if (Input.BtnDwn("Use" + i, GameMode.GAME_STATE.GAMEPLAY) && currentlyEquipped[i])
					currentlyEquipped[i].Use(0);
			}
		}

		for (int i = 0; i < handTarget.Length; i++)
			if (currentlyEquipped[i])
				handTarget[i].position = currentlyEquipped[i].gripType == GRIP_TYPE.ONE_HAND ? currentlyEquipped[i].grips_transform.GetChild(0).position : currentlyEquipped[i].grips_transform.GetChild(i).position;
	}

	private void SetCurrentlyEquipped(Equippable equippable_, int handIndex_)
	{
		for (int i = 0; i < (int)equippable_.gripType; i++)
		{
			bool _oneHand = equippable_ ? (equippable_.gripType == GRIP_TYPE.ONE_HAND) : true;
			int _hand = _oneHand ? handIndex_ : i;

			if (currentlyEquipped[_hand])
			{
				if (currentlyEquipped[_hand].gripType == GRIP_TYPE.ONE_HAND)
				{
					currentlyEquipped[_hand].rigidbody.isKinematic = false;
					currentlyEquipped[_hand].transform.parent = null;
					currentlyEquipped[_hand] = null;
				}
				else
				{
					for (int j = 0; j < 2; j++)
					{
						currentlyEquipped[j].rigidbody.isKinematic = false;
						currentlyEquipped[j].transform.parent = null;
						currentlyEquipped[j] = null;
					}
				}
			}

			if (equippable_)
			{
				equippable_.gameObject.SetActive(true);
				equippable_.rigidbody.isKinematic = true;
				equippable_.transform.parent = parent[_hand];
				equippable_.transform.localPosition = equippable_.defaultPose.position;
				equippable_.transform.localRotation = Quaternion.Euler(equippable_.defaultPose.rotation);
				equippable_.Equip(_hand);
			}

			currentlyEquipped[_hand] = equippable_;
			Collider[] _colliders = currentlyEquipped[_hand].GetComponentsInChildren<Collider>();
			foreach (Collider jc in GameMode.jeffrey.colliders)
				foreach (Collider ec in _colliders)
					Physics.IgnoreCollision(jc, ec, true);
		}
	}

	public bool Equip(Equippable equippable_, int handIndex_)
	{
		bool wasOccupied = currentlyEquipped[handIndex_];

		SetCurrentlyEquipped(equippable_, handIndex_);

		return wasOccupied;
	}
}