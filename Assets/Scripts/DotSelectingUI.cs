﻿using System;
using System.Collections;
using UnityEngine;

public class DotSelectingUI : MonoBehaviour
{
	[SerializeField] private Transform dot;
	[SerializeField] private float speed;
	[SerializeField] private float bckgrndRadius;
	[SerializeField] private float minLeanToSelect;
	[SerializeField] private DotChoiceUI[] choices;

	private DotChoiceUI target;
	public DotChoiceUI Target
	{
		get
		{
			DotChoiceUI _returnValue = target;
			return _returnValue;
		}
		set
		{
			if (target != DotChoiceUI.Invalid) target.representation.color = Color.black;
			target = value;
			if (target != DotChoiceUI.Invalid) target.representation.color = Color.green;
		}
	}

	private void Update()
	{
		Position();

		Selection();
	}

	private void Position()
	{
		//dot.localPosition += CustInput.Axis("Mouse X", GameMode.GAME_STATE.UI) * transform.right * speed * Time.deltaTime;
		//dot.localPosition += CustInput.Axis("Mouse Y", GameMode.GAME_STATE.UI) * transform.up * speed * Time.deltaTime;

		dot.localPosition = new Vector2(
			Input.Axs("DotSelectingXUI", GameMode.GAME_STATE.UI),
			Input.Axs("DotSelectingYUI", GameMode.GAME_STATE.UI)
			) * bckgrndRadius;

		float distance = Vector3.Distance(dot.localPosition, Vector3.zero);

		if (distance > bckgrndRadius)
			dot.localPosition *= bckgrndRadius / distance;
	}

	private void Selection()
	{
		float hor = dot.localPosition.x;
		float ver = dot.localPosition.y;
		float min = minLeanToSelect * bckgrndRadius;

		if (Mathf.Abs(hor) > min || Mathf.Abs(ver) > min)
		{
			if (Mathf.Abs(hor) > Mathf.Abs(ver))
			{
				if (hor > 0)
					Target = choices[1];
				else
					Target = choices[3];

			}
			else
			{
				if (ver > 0)
					Target = choices[0];
				else
					Target = choices[2];
			}
		}
		else
		{
			Target = DotChoiceUI.Invalid;
		}
	}
}