﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class QuestListEntryUI : MonoBehaviour, ISelectHandler
{
	[HideInInspector]
	public Quest QuestOnMe
	{
		get { return questOnMe; }
		set
		{
			text.text = value.questTitle;
			questOnMe = value;
		}
	}

	public Button button;
	[SerializeField] private Text text;
	[ReadOnly] [SerializeField] private Quest questOnMe;

	private void Start()
	{
		button.onClick.AddListener(() => QuestsJournalUI.SubscribeQuest(QuestOnMe));
	}

	public void OnSelect(BaseEventData eventData)
	{
		QuestsJournalUI.ShowQuestsContent(QuestOnMe);
	}
}