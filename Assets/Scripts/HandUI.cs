﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using static GameMode;

public class HandUI : MonoBehaviour
{
    private static HandUI ciop;
    public static int currHand;
    public static bool successful = false;

    [SerializeField] private Text amountText;
    [SerializeField] private Button body;

    private Coroutine coroutine;

    private void Awake()
    {
        ciop = this;
    }

    public static IEnumerator Show()
    {
        successful = false;
        yield return ciop.ShowOnInstance();
    }

    private IEnumerator ShowOnInstance()
    {
        currHand = 1;
        ChangeValue();
        body.Select();
        body.gameObject.SetActive(true);
        yield return coroutine = StartCoroutine(Counting());
    }

    public static void Close(bool successful)
    {
        ciop.body.gameObject.SetActive(false);
    }

    private IEnumerator Counting()
    {
        bool doLoop = true;

        while (doLoop)
        {
            float _value = Input.AxsRw("HorizontalUI", GAME_STATE.UI);
            if (_value != 0)
            {
                ChangeValue();
                while (Input.AxsRw("HorizontalUI", GAME_STATE.UI) == _value) yield return 0;
            }

            yield return 0;

            if (Input.BtnDwn("SubmitUI", GAME_STATE.UI) || Input.BtnDwn("CancelUI", GAME_STATE.UI))
            {
                Close(Input.BtnDwn("CancelUI", GAME_STATE.UI));
                doLoop = false;
            }
        }
    }

    private void ChangeValue()
    {
        currHand = currHand == 0 ? 1 : 0;
        amountText.text = currHand == 0 ? "LEFT" : "RIGHT";
    }
}