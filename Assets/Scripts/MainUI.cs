﻿using System;
using UnityEngine;

public class MainUI : MonoBehaviour
{
	private static MainUI ciop;

	public static WindowUI currWindow;

	[SerializeField] private DotSelectingUI dotSelecting;
	[SerializeField] private ActionsUI actionsUI;
	[SerializeField] private Color is_color;
	[SerializeField] private Color isnt_color;
	[SerializeField] private TradingUI tradingUI;

	private float prevMainUI;

	public bool Active
	{
		get { return Input.Axs("MainUI", GameMode.GAME_STATE.UI) == 1; }
		set
		{
			DotChoiceUI _target = dotSelecting.Target;
			GameMode.currState = value ? GameMode.GAME_STATE.UI : _target == DotChoiceUI.Invalid ? GameMode.GAME_STATE.GAMEPLAY : GameMode.currState;
			dotSelecting.transform.parent.gameObject.SetActive(value);
			dotSelecting.transform.localPosition = Vector3.zero;

			if (!value) OpenWindow(_target != DotChoiceUI.Invalid ? _target.window : null);
			dotSelecting.Target = DotChoiceUI.Invalid;
		}
	}

	private void Awake()
	{
		ciop = this;
	}

	private void Update()
	{
		if ((Input.Axs("MainUI", GameMode.GAME_STATE.UI) != prevMainUI || Input.Axs("MainUI", GameMode.GAME_STATE.GAMEPLAY) != prevMainUI) && !ItemDialogMenuUI.IsOpen)
		{
			Active = Input.Axs("MainUI") == 1;
		}
		prevMainUI = Input.Axs("MainUI");

		Back();

		HandleActions();
	}

	public static void OpenWindow(WindowUI newWindow)
	{
		if (newWindow == currWindow) return;

		if (currWindow)
			currWindow.CloseWindow();

		if (newWindow != null)
			newWindow.OpenWindow();

		currWindow = newWindow;

		GameMode.currState = currWindow ? GameMode.GAME_STATE.UI : GameMode.GAME_STATE.GAMEPLAY;
	}

	protected void Back()
	{
		if (Input.BtnDwn("CancelUI", GameMode.GAME_STATE.UI) && !ItemDialogMenuUI.IsOpen && currWindow)
		{
			OpenWindow(null);
			Active = false;
		}
	}

	private void HandleActions()
	{
		WorldObject hitWorldObject = Raycasting.hitWorldObject;
		actionsUI.Enabled = Raycasting.IsHitInRange && hitWorldObject;

		if (actionsUI.Enabled)
		{
			actionsUI.SetColor(1, hitWorldObject.talker ? is_color : isnt_color);
			actionsUI.SetColor(2, hitWorldObject.trader ? is_color : isnt_color);
			actionsUI.SetColor(3, hitWorldObject.pickUpable ? is_color : isnt_color);
			actionsUI.SetColor(4, hitWorldObject.collectable ? is_color : isnt_color);
			actionsUI.SetColor(5, hitWorldObject.interactable ? is_color : isnt_color);
		}
	}

	public static void StartTrading(Trader target)
	{
		TradingUI.target = target;
		OpenWindow(ciop.tradingUI);
	}
}