﻿using UnityEngine;
using UnityEngine.UI;

public class ActionsUI : MonoBehaviour
{
    [SerializeField] private Text[] actions_texts;

    private new bool enabled;
    public bool Enabled
    {
        get { return enabled; }

        set
        {
            enabled = value;
            gameObject.SetActive(value);
        }
    }
    
    public void SetColor(int index, Color newColor)
    {
        actions_texts[index].color = newColor;
    }
}