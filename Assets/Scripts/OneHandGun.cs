﻿using System;
using UnityEngine;

[Serializable]
public class OneHandGun : Gun
{
    //public override bool Fire()
    //{
    //    Debug.Log("BAM");

    //    return true;
    //}

	protected override bool Usage0()
	{
		return Fire();
	}

	protected override bool Usage1()
	{
		return Aim();
	}
}