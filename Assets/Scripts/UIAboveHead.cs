﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class UIAboveHead : MonoBehaviour
{
	public bool fadable;

	[SerializeField] protected Transform toFollow;
	[SerializeField] protected float timeBeforeFading = 5;
	[SerializeField] protected float timeOfFading = 3;
	[SerializeField] protected float fadingSpeed = 1;
	[SerializeField] private float yOffset;

	protected Coroutine fadeAwayCoroutine;

	protected abstract Graphic[] Graphics { get; }

	protected void LookAtTarget()
	{
		if (timeBeforeFading != 0 && timeOfFading != 0)
		{
			transform.LookAt(Camera.main.transform);
			transform.position = toFollow.position + yOffset * transform.up;
		}
	}

	protected void WakeUp()
	{
		float _timer = 0;
		for (int i = 0; i < Graphics.Length; i++)
		{
			_timer += Time.deltaTime;

			Color _color = Graphics[i].color;
			_color.a = 1;
			Graphics[i].color = _color;
		}

		if (fadeAwayCoroutine != null) StopCoroutine(fadeAwayCoroutine);
		fadeAwayCoroutine = StartCoroutine(FadeAway());
	}

	protected IEnumerator FadeAway()
	{
		while (!fadable) yield return 0;

		yield return new WaitForSeconds(timeBeforeFading);

		float _timer = 0;
		while (_timer < timeOfFading)
		{
			for (int i = 0; i < Graphics.Length; i++)
			{
				_timer += Time.deltaTime;

				Color _color = Graphics[i].color;
				_color.a -= fadingSpeed * Time.deltaTime;
				Graphics[i].color = _color;
			}

			yield return 0;
		}

		fadeAwayCoroutine = null;
	}
}