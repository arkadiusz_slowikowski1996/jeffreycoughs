﻿using System.Collections;
using UnityEngine;

public class Collecting : MonoBehaviour
{
    public delegate void CollectingEvent(string collectableName_, int amount_);
    public static event CollectingEvent Collected;

    [SerializeField] private float timeToHoldButtonDuring;

    private void Start()
    {
        StartCoroutine(CountTimeWhenPressed());
    }

    private IEnumerator CountTimeWhenPressed()
    {
        while (true)
        {
            if (Input.BtnDwn("Collect", GameMode.GAME_STATE.GAMEPLAY))
            {
                float _timer = 0;
                while (Input.Btn("Collect", GameMode.GAME_STATE.GAMEPLAY))
                {
                    _timer += Time.deltaTime;

                    if (_timer >= timeToHoldButtonDuring)
                        break;

                    yield return 0;
                }

                if (_timer <= timeToHoldButtonDuring)
                {
                    WorldObject worldObject = Raycasting.hitWorldObject;

                    if (Raycasting.IsHitInRange && worldObject)
                    {
                        Collectable collectable = worldObject.collectable;

                        if (collectable)
                        {
                            bool succ = GameMode.jeffrey.inventory.AddToInventory(collectable);

                            if (succ) Collected?.Invoke(collectable.colName, collectable.currStackSize);
                        }
                    }

                }
            }

            yield return 0;
        }
    }

    //private void Update()
    //{
    //	if (Input.BtnDwn("Collect", GameMode.GAME_STATE.GAMEPLAY))
    //	{
    //		WorldObject worldObject = Raycasting.hitWorldObject;
    //		if (Raycasting.IsHitInRange && worldObject)
    //		{
    //			Collectable collectable = worldObject.collectable;

    //			if (collectable)
    //			{
    //				bool succ = GameMode.jeffrey.inventory.AddToInventory(collectable);

    //				if (succ) Collected?.Invoke(collectable.colName, collectable.currStackSize);
    //			}
    //		}
    //	}
    //}
}