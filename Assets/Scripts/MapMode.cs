﻿#if UNITY_EDITOR
using System.Collections;
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapMode : MonoBehaviour
{
    public static MapMode i;

    public static PlayerSpawnPoint[] playerSpawnPoints;

    [SerializeField] private QuestGiver[] questGivers;

    private void Awake()
    {
        i = this;
        playerSpawnPoints = FindObjectsOfType<PlayerSpawnPoint>();
        questGivers = FindObjectsOfType<QuestGiver>();
	}

	private void Start()
	{
		StartCoroutine(StartCor());
	}

	private IEnumerator StartCor()
	{
		yield return 0;

		foreach (QuestGiver qg in questGivers)
			qg.AddTakableQuests();
	}
}