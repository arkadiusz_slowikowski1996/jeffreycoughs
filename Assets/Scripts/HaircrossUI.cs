﻿using UnityEngine;

public class HaircrossUI : MonoBehaviour
{
    [SerializeField] private RectTransform[] parts;
    [SerializeField] private Vector2[] directions;
    [SerializeField] private float multiplier;

    private Vector2[] startPoses;

    private void Start()
    {
        startPoses = new Vector2[parts.Length];

        for (int i = 0; i < parts.Length; i++)
            startPoses[i] = parts[i].localPosition;
    }

    private void Update()
    {
        for (int i = 0; i < parts.Length; i++)
            parts[i].localPosition = startPoses[i] + GameMode.jeffrey.disturbtionWhenMoving.Disturbtion * directions[i] * multiplier;
    }
}