﻿using System;
using UnityEngine;

public class InteractionReference : MonoBehaviour
{
	public static new Transform transform;
	public static Vector3 Position
	{
		get { return transform.position; }
	}

	[SerializeField] private Joint joint;
	[SerializeField] private float breakForce;

	private Vector3 startLocalPos;

	private void Start()
	{
		transform = base.transform;
		startLocalPos = transform.localPosition;
	}

	public Rigidbody ConnectedBody
	{
		get { return joint.connectedBody; }
	}

	public void SetConnectedBody(Rigidbody value, Vector3 point)
	{
		if (value)
		{
			transform.position = point;
		}
		else
		{
			transform.localPosition = startLocalPos;
		}

		DisableColliision(value);
		ChangeRigidbodiesCollisionDetection(value ? value : joint.connectedBody, value);

		///Bez tego obiekt zawisa w powietrzu kiedy klikamy PPM gdy sie nie rusza
		if (joint.connectedBody) joint.connectedBody.AddForce(transform.forward * 0.01f);

		joint.connectedBody = value;
	}

	private void FixedUpdate()
	{
		if (ConnectedBody && joint.currentForce.magnitude > breakForce)
		{
			ChangeRigidbodiesCollisionDetection(joint.connectedBody, false);
			SetConnectedBody(null, Vector3.zero);
		}
	}

	private void DisableColliision(Rigidbody value)
	{
		if (joint.connectedBody)
		{
			Collider[] conCols = joint.connectedBody.GetComponentsInChildren<Collider>();

			foreach (Collider jCol in GameMode.jeffrey.colliders)
			{
				foreach (Collider temp in conCols)
				{
					Physics.IgnoreCollision(jCol, temp, false);
				}
			}
		}

		if (value)
		{
			Collider[] valueCols = value.GetComponentsInChildren<Collider>();

			foreach (Collider jCol in GameMode.jeffrey.colliders)
			{
				foreach (Collider temp in valueCols)
				{
					Physics.IgnoreCollision(jCol, temp, true);
				}
			}
		}
	}

	private void ChangeRigidbodiesCollisionDetection(Rigidbody rigidbody, bool turnOn)
	{
		if (rigidbody)
		{
			rigidbody.collisionDetectionMode = turnOn ? CollisionDetectionMode.ContinuousSpeculative : CollisionDetectionMode.Discrete;
		}
	}
}