﻿using UnityEngine;
using UnityEngine.Serialization;

public class Jeffrey : Person
{
    public Animator animator;
    public Health health;
    public Movement movement;
    public PickingObjectsUp pickingObjectsUp;
    public Raycasting raycasting;
    public Equipping equipping;
    public Inventory inventory;
    public Collecting collecting;
    public DisturbtionWhenMoving disturbtionWhenMoving;

    public Collider[] colliders;

    private void OnValidate()
    {
        colliders = GetComponentsInChildren<Collider>();
    }
}