﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Gun : Equippable
{
    protected static float damageMultiplier = 0.05f;

    [Header("--GUN--")]
    public int currClipAmmo;              //Currently loaded ammo
    public int maxClipAmmo;               //Capacity of a clip
    public int allCurrAmmo;               //All ammo collected
    [SerializeField] protected EquippablePose[] recoilWaypoints;
    [SerializeField] protected EquippablePose[] reloadWaypoints;
    [SerializeField] protected float explosionForce;

    protected float currRecoilTime;
    protected Coroutine recoilCoroutine;
    protected Coroutine reloadCoroutine;
    protected Coroutine addExtraDisturbtionCoroutine;

    protected float RecoilTime
    {
        get
        {
            float _returnValue = 0;
            for (int i = 0; i < recoilWaypoints.Length; i++)
                _returnValue += recoilWaypoints[i].time;

            return _returnValue;
        }
    }

    public override void Equip(int at_)
    {
        base.Equip(at_);

        ClipStateUI.UpdateState(EquippedAt, this);
    }

    public virtual bool Aim()
    {
        //TODO: lower camera's fov

        return true;
    }

    public virtual bool Fire()
    {
        if (reloadCoroutine == null)
        {
            if (currClipAmmo > 0)
            {
                currClipAmmo--;

                Shoot();

                if (recoilCoroutine != null)
                {
                    transform.localPosition = defaultPose.position;
                    transform.localEulerAngles = defaultPose.rotation;
                }
                recoilCoroutine = StartCoroutine(MoveOnTrack(recoilWaypoints, () => { recoilCoroutine = null; return 0; }));

                ClipStateUI.UpdateState(EquippedAt, this);

                return true;
            }

            ClipStateUI.ShowEvent(EquippedAt, false);
        }

        return false;
    }

    protected virtual void Shoot()
    {
        //Debug.Log("Create Explosion");
        //Collider[] _colliders = Physics.OverlapSphere(Raycasting.currentHit.point, explosionRadius);
        //Debug.Log(_colliders.Length);
        //foreach (Collider c in _colliders)
        //{
        //    if (c.attachedRigidbody)
        //    {
        //        Debug.Log("Explosion Force added for " + c.attachedRigidbody.name);
        //        c.attachedRigidbody.AddExplosionForce(explosionForce, Raycasting.currentHit.point, explosionRadius);
        //    }
        //}

        RaycastHit _hit;
        Raycasting.RaycastFromCamera(out _hit, GameMode.jeffrey.disturbtionWhenMoving.GetDisturbionWithExtra(currRecoilTime / RecoilTime));

        //GameObject _shot = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //_shot.transform.localScale = Vector3.one * 0.2f;
        //_shot.transform.position = _hit.point;

        if (addExtraDisturbtionCoroutine != null) StopCoroutine(addExtraDisturbtionCoroutine);
        addExtraDisturbtionCoroutine = StartCoroutine(AddExtraDisturbtion());
        if (_hit.collider && _hit.collider.attachedRigidbody)
        {
            Vector3 _dir = (_hit.point - Raycasting.Origin).normalized;
            _hit.collider.attachedRigidbody.AddForce(_dir * explosionForce);
            _hit.collider.GetComponent<Health>()?.TakeDamage((int)(explosionForce * damageMultiplier));
        }
    }

    public bool Reload()
    {
        if (allCurrAmmo > 0 && currClipAmmo < maxClipAmmo && reloadCoroutine == null)
        {
            int _ammoReloaded = maxClipAmmo - currClipAmmo;
            if (allCurrAmmo - _ammoReloaded < 0) _ammoReloaded = allCurrAmmo;
            allCurrAmmo -= _ammoReloaded;
            currClipAmmo += _ammoReloaded;

            reloadCoroutine = StartCoroutine(MoveOnTrack(reloadWaypoints, () => { reloadCoroutine = null; return 0; }));

            ClipStateUI.UpdateState(EquippedAt, this);
            ClipStateUI.ShowEvent(EquippedAt, true);

            return true;
        }
        ClipStateUI.ShowEvent(EquippedAt, false);
        return false;
    }

    //protected IEnumerator Recoil()
    //{
    //	while (currRecoilTime < RecoilTime / 2f)
    //	{
    //		currRecoilTime += Time.deltaTime;
    //		transform.localPosition = Vector3.Lerp(defaultEquippedPosition, recoilPoses, currRecoilTime / (RecoilTime / 2f));
    //		transform.localEulerAngles = Vector3.Lerp(defaultEquippedRotation, recoilRots, currRecoilTime / (RecoilTime / 2f));

    //		yield return 0;
    //	}

    //	while (currRecoilTime < RecoilTime)
    //	{
    //		currRecoilTime += Time.deltaTime;
    //		transform.localPosition = Vector3.Lerp(recoilPoses, defaultEquippedPosition, currRecoilTime / RecoilTime);
    //		transform.localEulerAngles = Vector3.Lerp(recoilRots, defaultEquippedRotation, currRecoilTime / RecoilTime);

    //		yield return 0;
    //	}

    //	recoilCoroutine = null;
    //	currRecoilTime = 0;
    //}

    protected IEnumerator AddExtraDisturbtion()
    {
        yield return 0;
        while (currRecoilTime != 0)
        {
            GameMode.jeffrey.disturbtionWhenMoving.extra = Mathf.Clamp(1 - (currRecoilTime / RecoilTime), 0, 1);
            yield return 0;
        }
    }
}

[Serializable]
public class EquippablePose
{
    public Vector3 position;
    public Vector3 rotation;
    public float time;
}