﻿using UnityEngine;

public class FollowPosition : MonoBehaviour
{
    [SerializeField] private Transform toFollow;

    private void Update()
    {
        transform.position = toFollow.position;
    }
}