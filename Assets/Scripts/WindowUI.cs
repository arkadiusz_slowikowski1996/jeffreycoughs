﻿using UnityEngine;
using UnityEngine.UI;

public abstract class WindowUI : MonoBehaviour
{
	public static WindowUI windowOpened;

	public WindowUI prevWindow;

	[SerializeField] protected Button defaultFocusTarget;
	[SerializeField] private GameObject window_go;

	protected abstract void OnWindowOpened();
	protected abstract void OnWindowClosed();

	public void Focus()
	{
		if (defaultFocusTarget) defaultFocusTarget.Select();
	}

	public void Refresh()
	{
		OnWindowOpened();
	}

	public void OpenWindow()
	{
		window_go.SetActive(true);
		OnWindowOpened();
		Focus();
	}

	public void CloseWindow()
	{
		window_go.SetActive(false);
		OnWindowClosed();
	}
}