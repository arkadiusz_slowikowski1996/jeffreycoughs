﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Collectable;

[RequireComponent(typeof(WorldObject))]
[RequireComponent(typeof(Inventory))]
public class Trader : WorldObjectComponent
{
	public enum REASONS { NO_REASON, NOT_ENOUGH_MONEY, NOT_INTERESTED_IN }

	public Inventory inventory;

	[SerializeField] private ItemCategory_Trader[] acceptedCategories = new ItemCategory_Trader[0];

	//public Collectable Exchange(Collectable collectable_, int amount_, Collectable cashInExchange_)
	//{
	//    int _index = inventory.GetIndexOf(collectable_);
	//    bool _last;

	//    Collectable _returnValue = inventory.RemoveFromInventory(ref collectable_, amount_, out _last);

	//    return _returnValue;
	//}

	public bool CanBeExchanged(string requestedName_, int amount_, Collectable inExchange_, out REASONS reason_)
	{
		reason_ = REASONS.NO_REASON;
		bool _atLeastOneCategory = false;
		foreach (ItemCategory_Trader ict in acceptedCategories)
			if (ict.category == inExchange_.category)
			{
				_atLeastOneCategory = true;
				break;
			}

		if (!_atLeastOneCategory)
		{
			reason_ = REASONS.NOT_INTERESTED_IN;
			return false;
		}

		int _index = inventory.GetIndexOf(requestedName_);
		if (_index != -1)
		{
			if (inventory.content[_index].currStackSize >= amount_)
			{
				Debug.Log("I GOT THE MONY");
				return true;
			}
			else
			{
				reason_ = REASONS.NOT_ENOUGH_MONEY;
				return false;
			}
		}

		reason_ = REASONS.NOT_ENOUGH_MONEY;
		return false;
	}

	public Collectable Exchange(Collectable requested_, int amount_, Collectable inExchange_)
	{
		int _index = inventory.GetIndexOf(requested_);
		bool _last;

		Collectable _returnValue = inventory.RemoveFromInventory(requested_, amount_, out _last);
		inventory.AddToInventory(inExchange_);

		if (requested_.currStackSize == 0) Destroy(requested_.gameObject);

		return _returnValue;
	}

	public bool IsAccepted(CATEGORY category_)
	{
		if (acceptedCategories.Length == 0)
			return true;

		return acceptedCategories.Contains(category_);
	}

	public int MoneyFor(Collectable collectable, bool buy)
	{
		foreach (ItemCategory_Trader ict in acceptedCategories)
		{
			if (ict.category == collectable.category)
			{
				return collectable.defaultValue * Mathf.FloorToInt((buy ? ict.buyMultipier : ict.sellMultipier));
			}
		}
		Debug.LogError("BŁĄD");
		return collectable.defaultValue;
	}

	protected override void OnValidate()
	{
		base.OnValidate();

		if (!inventory) inventory = GetComponent<Inventory>();
	}
}

[Serializable]
public class ItemCategory_Trader
{
	public CATEGORY category;
	public float buyMultipier = 1;
	public float sellMultipier = 1;
}