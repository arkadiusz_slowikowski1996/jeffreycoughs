﻿using System;
using System.Collections;
using UnityEngine;

[Serializable]
public abstract class Equippable : Collectable
{
    public enum GRIP_TYPE { ONE_HAND = 1, TWO_HAND = 2 }
    [Header("--EQUIPPABLE--")]
    public GRIP_TYPE gripType;

    public Transform grips_transform;
    public EquippablePose defaultPose;

    [SerializeField] protected float[] use_cooldown = new float[2];
    [SerializeField] protected float[] use_currCooldown = new float[2];

    //protected Func<bool>[] usages = new Func<bool>[2];

    [SerializeField] private int equippedAt = -1;

    protected int EquippedAt
    {
        get { return equippedAt; }
        set
        {
            gameObject.SetActive(value != -1);
            equippedAt = value;
        }
    }

    protected bool Equipped
    {
        get { return EquippedAt != -1; }
    }

    protected abstract bool Usage0();
    protected abstract bool Usage1();

    private void Update()
    {
        for (int i = 0; i < 2; i++)
            if (Equipped)
                Cooldown(i);
    }

    private void OnValidate()
    {
        gripType = (GRIP_TYPE)grips_transform.childCount;
    }

    public virtual void Equip(int at_)
    {
        EquippedAt = at_;
    }

    public bool Use(int use)
    {
        if (Cooldowned(0))
        {
            if (use == 0)
                Usage0();
            else if (use == 1)
                Usage1();
            else
                Debug.LogError("ZJEB");

            SetCooldown(use);

            return true;
        }
        else
        {
            return false;
        }
    }

    protected bool Cooldowned(int use)
    {
        if (use < 2)
        {
            return use_currCooldown[use] == 0;
        }
        else
        {
            Debug.Log("ZJEB");
            return false;
        }
    }

    protected void SetCooldown(int use)
    {
        if (use < 2)
            use_currCooldown[use] = use_cooldown[use];
        else
            Debug.Log("ZJEB");
    }

    protected void Cooldown(int use)
    {
        if (use < 2)
        {
            if (use_currCooldown[use] > 0)
                use_currCooldown[use] -= Time.deltaTime;
            else
                use_currCooldown[use] = 0;
        }
        else
        {
            Debug.Log("ZJEB");
        }
    }

    protected IEnumerator MoveOnTrack(EquippablePose[] points_, Func<int> todoAfter_)
    {
        for (int _curr = 0; _curr < points_.Length; _curr++)
        {
            float _timer = 0;

            EquippablePose _start = _curr > 0 ? points_[_curr - 1] : points_[points_.Length - 1];
            EquippablePose _dest = points_[_curr];

            while (_timer < points_[_curr].time)
            {
                _timer += Time.deltaTime;

                transform.localPosition = Vector3.Lerp(_start.position, _dest.position, _timer / _dest.time);
                transform.localEulerAngles = Vector3.Lerp(_start.rotation, _dest.rotation, _timer / _dest.time);

                yield return 0;
            }
        }

        todoAfter_();
    }
}