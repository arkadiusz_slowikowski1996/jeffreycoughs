﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
	public static IEnumerator SetCollisionDetectionForTime(this Rigidbody rigidbody, CollisionDetectionMode collisionDetectionMode, float time)
	{
		CollisionDetectionMode defaultMode = rigidbody.collisionDetectionMode;
		rigidbody.collisionDetectionMode = collisionDetectionMode;
		yield return new WaitForSeconds(time);
		rigidbody.collisionDetectionMode = defaultMode;
	}

	public static bool Contains(this ItemCategory_Trader[] array, Collectable.CATEGORY category)
	{
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i].category == category)
				return true;
		}

		return false;
	}

	public static Quest GetQuest(this List<Quest> list_, string questTitle_)
	{
		foreach (Quest q in list_)
		{
			if (q.questTitle == questTitle_)
				return q;
		}

		return null;
	}

	public static void ClearWithDestroy(this List<MonoBehaviour> list_)
	{
		foreach (MonoBehaviour mb in list_)
			Object.Destroy(mb.gameObject);
		list_.Clear();
	}

	public static void ClearWithDestroy(this MonoBehaviour[] array_)
	{
		if (array_ != null)
		{
			foreach (MonoBehaviour mb in array_)
				Object.Destroy(mb.gameObject);
			array_ = null;
		}
	}
}