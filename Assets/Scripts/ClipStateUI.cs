﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClipStateUI : MonoBehaviour
{
	private static ClipStateUI instance;

	[SerializeField] private Text[] currAmmoTexts;
	[SerializeField] private Text[] restOfTheAmmo;
	[SerializeField] private Color originalColor;
	[SerializeField] private Color badEventColor;
	[SerializeField] private Color goodEventColor;
	[SerializeField] private float timeOfShowinEvent;

	private static Coroutine showingErrorCoroutine;

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		for (int i = 0; i < 2; i++)
			UpdateState(i, GameMode.jeffrey.equipping.currentlyEquipped[i] ? GameMode.jeffrey.equipping.currentlyEquipped[i] as Gun : null);
	}

	public static void UpdateState(int hand_, Gun gun_)
	{
		if (gun_ && gun_.gripType == Equippable.GRIP_TYPE.TWO_HAND)
			hand_ = 0;

		instance.currAmmoTexts[hand_].text = gun_ ? gun_.currClipAmmo.ToString() : "";
		instance.restOfTheAmmo[hand_].text = gun_ ? "/" + gun_.allCurrAmmo.ToString() : "";
	}

	public static void ShowEvent(int hand_, bool isItGoodEvent_)
	{
		if (showingErrorCoroutine != null) instance.StopCoroutine(showingErrorCoroutine);
		showingErrorCoroutine = instance.StartCoroutine(instance.ShowingError(hand_, isItGoodEvent_ ? instance.goodEventColor : instance.badEventColor));
	}

	private IEnumerator ShowingError(int hand_, Color color_)
	{
		float _timer = 0;
		while (_timer < timeOfShowinEvent)
		{
			_timer += Time.deltaTime;

			instance.currAmmoTexts[hand_].color = Color.Lerp(color_, originalColor, _timer / timeOfShowinEvent);
			instance.restOfTheAmmo[hand_].color = Color.Lerp(color_, originalColor, _timer / timeOfShowinEvent);

			yield return 0;
		}
	}
}