﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowingNameplate : WorldObjectComponent
{
    [SerializeField] private Transform toFollowForNameplate;
    [SerializeField] private Nameplate nameplatePrefab;
    [HideInInspector] public Nameplate nameplate;

    private void Start()
    {
        if (nameplatePrefab)
        {
            nameplate = Instantiate(nameplatePrefab, GameMode.WorldCanvas.transform);
            nameplate.SetValues(worldObject.objName, toFollowForNameplate);
        }
    }
}