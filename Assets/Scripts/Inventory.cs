﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public Collectable[] content;

    [SerializeField] private int inventorySize;
    [SerializeField] private Transform content_transform;

    public int SpaceLeft
    {
        get
        {
            int returnValue = 0;
            for (int i = 0; i < content.Length; i++)
            {
                if (content[i] == null) returnValue++;
            }
            return returnValue;
        }
    }

    private void Start()
    {
        if (content.Length == 0)
        {
            content = new Collectable[inventorySize];
        }
        else
        {
            Collectable[] newContent = new Collectable[inventorySize];
            for (int i = 0; i < newContent.Length; i++)
            {
                newContent[i] = i < content.Length ? content[i] : null;
            }
            content = newContent;
        }
    }

    public bool AddToInventory(Collectable collectable_)
    {
        int ind = GetIndexOf(collectable_.colName);

        if (ind != -1)
        {
            if (content[ind].currStackSize + 1 > content[ind].maxStackSize)
            {
                ind = -1;
            }
            else
            {
                content[ind].currStackSize += collectable_.currStackSize;
                Destroy(collectable_.gameObject);
            }
        }

        if (ind == -1)
        {
            int lastOccupied = GetLastOccupied();

            if (lastOccupied > -1)
            {
                content[lastOccupied] = collectable_;
                collectable_.rigidbody.isKinematic = true;
                collectable_.transform.parent = content_transform;
                collectable_.gameObject.SetActive(false);
                collectable_.transform.position = transform.position;
            }
            else
            {
				return false;
                //TODO: run error that no place in inventory
            }
            //TODO: Run event that item has been added, so UI knows about that
        }

		return true;
    }

    //public Collectable RemoveFromInventory(ref Collectable collectable_, int amount_, out bool last_)
    //{
    //    int indexOf = GetIndexOf(collectable_);

    //    last_ = content[indexOf].currStackSize - amount_ == 0;

    //    if (amount_ == 1)
    //    {
    //        //Dont create bundle
    //    }
    //    else
    //    {
    //        //Create bundle
    //    }

    //    if (last_)
    //    {
    //        content[indexOf] = null;

    //        for (int i = indexOf + 1; i < content.Length; i++)
    //        {
    //            content[i - 1] = content[i] ? content[i] : null;
    //        }
    //    }
    //    else
    //    {
    //        collectable_.currStackSize -= amount_;
    //        Collectable newInstance = Instantiate(collectable_, null);
    //        newInstance.currStackSize = amount_;
    //        collectable_ = newInstance;
    //    }

    //    return collectable_;
    //}

    public Collectable RemoveFromInventory(Collectable collectable_, int amount_, out bool last_)
    {
        Collectable returnValue;

        int indexOf = GetIndexOf(collectable_);

        last_ = content[indexOf].currStackSize - amount_ == 0;

        if (amount_ == 1)
        {
            //Dont create bundle
        }
        else
        {
            //Create bundle
        }

        if (last_)
        {
            returnValue = content[indexOf];
            returnValue.transform.parent = null;
            content[indexOf] = null;

            for (int i = indexOf + 1; i < content.Length; i++)
            {
                content[i - 1] = content[i] ? content[i] : null;
            }
        }
        else
        {
            collectable_.currStackSize -= amount_;
            returnValue = Instantiate(collectable_, null);
            returnValue.currStackSize = amount_;
        }

        return returnValue;
    }

    public Collectable RemoveFromInventory(string collectableName_, int amount_, out bool last_)
    {
        Collectable returnValue;

        int indexOf = GetIndexOf(collectableName_);

        last_ = content[indexOf].currStackSize - amount_ == 0;

        if (amount_ == 1)
        {
            //Dont create bundle
        }
        else
        {
            //Create bundle
        }

        if (last_)
        {
            returnValue = content[indexOf];
            content[indexOf] = null;

            for (int i = indexOf + 1; i < content.Length; i++)
            {
                content[i - 1] = content[i] ? content[i] : null;
            }
        }
        else
        {
            content[indexOf].currStackSize -= amount_;
            returnValue = Instantiate(content[indexOf], null);
            returnValue.currStackSize = amount_;
        }

        return returnValue;
    }

    public int GetIndexOf(string name)
    {
        for (int i = 0; i < content.Length; i++)
        {
            if (content[i] == null) break;
            if (name == content[i].colName)
            {
                return i;
            }
        }

        return -1;
    }

    public int GetIndexOf(Collectable collectable)
    {
        return GetIndexOf(collectable.colName);

        //for (int i = 0; i < content.Length; i++)
        //{
        //    if (content[i] == collectable)
        //    {
        //        return i;
        //    }
        //}

        //return -1;
    }

    private int GetLastOccupied()
    {
        for (int i = 0; i < content.Length; i++)
        {
            if (content[i] == null)
                return i;
        }

        return -1;
    }
}