﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Radar : MonoBehaviour
{
    private static Radar ciop;

    [SerializeField] private Image icon_prefab;
    [SerializeField] private Transform icons_parent;
    [SerializeField] private float xPosScale;

    private void Awake()
    {
        ciop = this;
    }

    private void Update()
    {
        for (int i = 0; i < Radaring.radarObjects.Count; i++)
        {
            Radaring.isIn[i] = Radaring.CheckIfInTriangle(Radaring.Position, Radaring.Positions[0], Radaring.Positions[1], Radaring.radarObjects[i].Position);

            if (Radaring.isIn[i])
            {
                if (!Radaring.radarObjects[i].myIcon) SpawnIcon(Radaring.radarObjects[i]);

                ///Check how far on right
                Vector3 locPos = Radaring.InverseTransformPoint(Radaring.radarObjects[i].Position);
                locPos.x *= xPosScale;
                Radaring.radarObjects[i].myIcon.transform.position = transform.TransformPoint(locPos);
                ///Check how far on forward
                Radaring.radarObjects[i].myIcon.rectTransform.localScale = Vector3.one * (1 - Vector3.Distance(Radaring.Position, Radaring.radarObjects[i].Position) / Mathf.Sqrt(Mathf.Pow(Radaring.ZRange, 2) + Mathf.Pow(Radaring.XRange, 2)));
            }
            else if (Radaring.radarObjects[i].myIcon)
            {
                Destroy(Radaring.radarObjects[i].myIcon.gameObject);
            }
        }

        SortIcons();
    }

    public static void SpawnIcon(RadarObject radarObject_)
    {
        Image icon = Instantiate(radarObject_.myIconPrefab ? radarObject_.myIconPrefab : ciop.icon_prefab, ciop.icons_parent);
        icon.name = radarObject_.name;
        icon.color = radarObject_.icon_color;
        radarObject_.myIcon = icon;
    }

    public void SortIcons()
    {
        List<RadarObject> _temp = new List<RadarObject>();

        foreach (RadarObject ro in Radaring.radarObjects)
            if (ro.myIcon)
                _temp.Add(ro);

        _temp = _temp.OrderByDescending((o) => Vector3.Distance(o.Position, Radaring.Position)).ToList();

        for (int i = 0; i < _temp.Count; i++)
            _temp[i].myIcon.transform.SetSiblingIndex(i);
    }
}