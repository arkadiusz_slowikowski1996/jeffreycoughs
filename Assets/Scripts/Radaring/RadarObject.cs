﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public delegate void SpawningRadarObjects(RadarObject radarObject_);

public class RadarObject : MonoBehaviour
{
    public static event SpawningRadarObjects RadarObjectSpawned;
    public static event SpawningRadarObjects RadarObjectDestroyed;

    [HideInInspector] public Image myIcon;
    public Image myIconPrefab;
    public Color icon_color;

    public Vector3 Position { get { return transform.position; } }

    private void Start()
    {
        StartCoroutine(WaitUntilSomeoneSubscribes(() =>
        {
            RadarObjectSpawned(this);
            return 0;
        }));
    }

    private void OnDestroy()
    {
        RadarObjectDestroyed(this);
    }

    private IEnumerator WaitUntilSomeoneSubscribes(Func<int> toDo)
    {
        while (RadarObjectSpawned == null)
            yield return 0;

        toDo();
    }
}