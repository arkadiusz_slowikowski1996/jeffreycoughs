﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Radaring : MonoBehaviour
{
    public static Vector3 Position { get { return ciop.transform.position; } }
    public static Vector3 Right { get { return ciop.transform.right; } }
    public static Vector3 Forward { get { return ciop.transform.forward; } }
    public static bool[] isIn;
    public static List<RadarObject> radarObjects = new List<RadarObject>();
    public static float XRange { get { return ciop.xRange; } }
    public static float ZRange { get { return ciop.zRange; } }

    private static Radaring ciop;

    [SerializeField] private float xRange;
    [SerializeField] private float zRange;
    [SerializeField] private bool debug;

    private Transform[] edges = new Transform[2];

    public static Vector3[] Positions
    {
        get
        {
            Vector3[] _returnValue = new Vector3[2];
            _returnValue[0] = Position + ciop.zRange * Forward + -ciop.xRange * Right;
            _returnValue[1] = Position + ciop.zRange * Forward + ciop.xRange * Right;

            return _returnValue;
        }
    }

    private void Awake()
    {
        ciop = this;
        RadarObject.RadarObjectSpawned += RadarObject_RadarObjectSpawned;
        RadarObject.RadarObjectDestroyed += RadarObject_RadarObjectDestroyed; 
    }

    private void Start()
    {
        if (debug)
        {
            for (int i = 0; i < edges.Length; i++)
            {
                edges[i] = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
                Destroy(edges[i].GetComponent<Collider>());
                edges[i].localScale /= 4;
            }
        }
    }

    private void Update()
    {
        if (debug)
        {
            edges[0].position = Positions[0];
            edges[1].position = Positions[1];
        }
    }

    public static Vector3 InverseTransformPoint(Vector3 position_)
    {
        return ciop.transform.InverseTransformPoint(position_);
    }

    public static bool CheckIfInTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 p)
    {
        float x1 = v1.x, z1 = v1.z;
        float x2 = v2.x, z2 = v2.z;
        float x3 = v3.x, z3 = v3.z;

        double x = p.x;
        double z = p.z;

        double a = ((z2 - z3) * (x - x3) + (x3 - x2) * (z - z3)) / ((z2 - z3) * (x1 - x3) + (x3 - x2) * (z1 - z3));
        double b = ((z3 - z1) * (x - x3) + (x1 - x3) * (z - z3)) / ((z2 - z3) * (x1 - x3) + (x3 - x2) * (z1 - z3));
        double c = 1 - a - b;

        if (a == 0 || b == 0 || c == 0) return false;//Debug.Log("Point is on the side of the triangle");
        else if (a >= 0 && a <= 1 && b >= 0 && b <= 1 && c >= 0 && c <= 1) return true; //Debug.Log("Point is inside of the triangle. " + a + " " + b + " " + c);
        else return false; //Debug.Log("Point is outside of the triangle.");
    }

    private void RadarObject_RadarObjectSpawned(RadarObject radarObject_)
    {
        radarObjects.Add(radarObject_);
        isIn = new bool[radarObjects.Count];
    }

    private void RadarObject_RadarObjectDestroyed(RadarObject radarObject_)
    {
    }
}