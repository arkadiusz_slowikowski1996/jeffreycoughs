﻿using UnityEngine;
using static GameMode;

public static class Input
{
    public static float Axs(string axisName, GAME_STATE gameState)
    {
        return gameState == currState ? UnityEngine.Input.GetAxis(axisName) : 0;
    }

    public static float Axs(string axisName)
    {
        return UnityEngine.Input.GetAxis(axisName);
    }

    public static float AxsRw(string axisName, GAME_STATE gameState)
    {
        return gameState == currState ? UnityEngine.Input.GetAxisRaw(axisName) : 0;
    }

    public static float AxsRw(string axisName)
    {
        return UnityEngine.Input.GetAxisRaw(axisName);
    }

    public static bool Btn(string btnName, GAME_STATE gameState)
    {
        return gameState == currState ? UnityEngine.Input.GetButton(btnName) : false;
    }

    public static bool Btn(string btnName)
    {
        return UnityEngine.Input.GetButton(btnName);
    }

    public static bool BtnDwn(string btnName, GAME_STATE gameState)
    {
        return gameState == currState ? UnityEngine.Input.GetButtonDown(btnName) : false;
    }

    public static bool BtnDwn(string btnName)
    {
        return UnityEngine.Input.GetButtonDown(btnName);
    }

    public static bool BtnUp(string btnName, GAME_STATE gameState)
    {
        return gameState == currState ? UnityEngine.Input.GetButtonUp(btnName) : false;
    }

    public static bool BtnUp(string btnName)
    {
        return UnityEngine.Input.GetButtonUp(btnName);
    }
}