﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using static GameMode;

public class QuestsJournalUI : JournalWindowUI
{
    private enum SHOWN_QUESTS { ACTIVE, FINISHED }

    private static QuestsJournalUI instance;

    [SerializeField] private Text questsTypeText;
    [SerializeField] private Text questsContentText;
    [SerializeField] private Text questsNameText;
    [SerializeField] private Scrollbar[] scrolls = new Scrollbar[2];
    [SerializeField] private float[] scrollMultipliers = new float[2];
    [SerializeField] private QuestListEntryUI questListEntryPrefab;
    [SerializeField] private Transform questListEntriesParent;

    private QuestListEntryUI[] currentQuestsEntries;
    private QuestListEntryUI[] finishedQuestsEntries;
    private float timeBeforeRepeat = 0.02f;
    private float repeatTime = 0.1f;
    private int iterations0 = 30;
    private int iterations1 = 100;
    private float mul0 = 0.5f;
    private float mul1 = 0.1f;
    private float prevHor;

    private SHOWN_QUESTS shownQuests;
    private SHOWN_QUESTS ShownQuests
    {
        get { return shownQuests; }
        set
        {
            shownQuests = value;
            List<Quest> _questsToShow = shownQuests == SHOWN_QUESTS.ACTIVE ? Questing.currentQuests : Questing.finishedQuests;
            ShowQuestsList(ref _questsToShow);
            questsTypeText.text = shownQuests.ToString();

            JournalUI.FocusStatic();
        }
    }

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        ///Scroll quest's content
		scrolls[1].value += Input.Axs("VerticalUI1", GAME_STATE.UI) * scrollMultipliers[1] * Time.deltaTime;

        ///Switch type of shown quests
        float axisRaw = Input.AxsRw("HorizontalUI", GAME_STATE.UI);
        if (axisRaw != prevHor && axisRaw != 0)
            ShownQuests = ShownQuests == SHOWN_QUESTS.ACTIVE ? SHOWN_QUESTS.FINISHED : SHOWN_QUESTS.ACTIVE;

        prevHor = axisRaw;
    }

    public static void ShowQuestsContent(Quest quest_)
    {
        StringBuilder _toShow = new StringBuilder();
        foreach (Line l in quest_.takingQuestLines)
            _toShow.Append(string.Format("{0}: {1} \n", l.author, l.text));

        instance.questsContentText.text = _toShow.ToString();
        instance.questsNameText.text = quest_.questTitle;
    }

    public static void SubscribeQuest(Quest quest_)
    {
        ActiveQuestUI.Show(quest_);
    }

    private void ShowQuestsList(ref List<Quest> quests_)
    {
        questsContentText.text = "";
        questsNameText.text = "";
        for (int i = 0; i < scrolls.Length; i++)
            scrolls[i].value = 0;
        currentQuestsEntries.ClearWithDestroy();

        quests_ = quests_.OrderBy((o) => o.questTitle).ToList();
        for (int i = 0; i < quests_.Count; i++)
        {
            if (quests_[i].mainQuest)
            {
                Quest _temp = quests_[i];
                quests_.RemoveAt(i);
                quests_.Insert(0, _temp);
            }
        }

        currentQuestsEntries = new QuestListEntryUI[quests_.Count];
        for (int i = 0; i < quests_.Count; i++)
        {
            currentQuestsEntries[i] = Instantiate(questListEntryPrefab, questListEntriesParent);
            currentQuestsEntries[i].transform.localPosition = new Vector3(
                0, questListEntryPrefab.GetComponent<RectTransform>().rect.height * -i,
                0
                );
            currentQuestsEntries[i].QuestOnMe = quests_[i];

            if (i > 0)
            {
                Navigation _nav0 = currentQuestsEntries[i - 1].button.navigation;
                _nav0.selectOnDown = currentQuestsEntries[i].button;
                currentQuestsEntries[i - 1].button.navigation = _nav0;
                Navigation _nav1 = currentQuestsEntries[i].button.navigation;
                _nav1.selectOnUp = currentQuestsEntries[i - 1].button;
                currentQuestsEntries[i].button.navigation = _nav1;
            }
            else
            {
                JournalUI.DefaultFocusTarget = currentQuestsEntries[i].button;
            }
        }
    }

    public override void OnWindowOpened()
    {
        ShownQuests = SHOWN_QUESTS.ACTIVE;

        ///TODO: FILL WITH JEFFREYS QUEST LISTS
    }

    public override void OnWindowClosed()
    {
    }



    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private RectTransform contentPanel;
    private void SnapTo(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();

        contentPanel.anchoredPosition =
            (Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position)
            - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
    }
}