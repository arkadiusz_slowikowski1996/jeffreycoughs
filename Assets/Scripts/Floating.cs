﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
    [SerializeField] private float goingTime;
    [SerializeField] private float dif;
    [SerializeField] private bool debug;
    private float dest;

    private float firstPos;

    private void Start()
    {
        firstPos = transform.position.y;
        dest = firstPos + dif;
        StartCoroutine(Float());
    }

    private IEnumerator Float()
    {
        bool goUp = true;

        while (true)
        {
            float timer = 0;
            float startPos = goUp ? firstPos : dest;
            float finalPos = goUp ? dest : firstPos;

            while (timer < goingTime)
            {
                transform.position = new Vector3(transform.position.x, Mathf.Lerp(startPos, finalPos, timer / goingTime), transform.position.z);
                timer += Time.deltaTime;

                yield return 0;
            }

            goUp = !goUp;

            yield return 0;
        }
    }
}