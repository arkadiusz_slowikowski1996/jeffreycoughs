﻿using UnityEngine;

public class GameMode : MonoBehaviour
{
	public enum GAME_STATE { GAMEPLAY, UI, MENU, TALKING }

	public static Jeffrey jeffrey;
	public static GAME_STATE currState;

	public GAME_STATE currState_;

	private static GameMode instance;

	[SerializeField] private Jeffrey jeffrey_prefab;
	[SerializeField] private Canvas worldCanvas;

	public static Canvas WorldCanvas { get { return instance.worldCanvas; } }

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		BlockCursor();

		jeffrey = Instantiate(
			jeffrey_prefab,
			MapMode.playerSpawnPoints[0].transform.position,
			MapMode.playerSpawnPoints[0].transform.rotation,
			null
			);
	}

	private void Update()
	{
		currState_ = currState;
	}

	private void BlockCursor()
	{
		Cursor.visible = !Cursor.visible;
		Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
	}
}