﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct DotChoiceUI
{
	public static DotChoiceUI Invalid = new DotChoiceUI(null, null);

	public WindowUI window;
	public Text representation;

	public bool IsValid
	{
		get { return window != null && representation != null; }
	}

	public DotChoiceUI(WindowUI windowUI_, Text representation_)
	{
		window = windowUI_;
		representation = representation_;
	}

	public static bool operator ==(DotChoiceUI dotChoiceUI0, DotChoiceUI dotChoiceUI1)
	{
		return dotChoiceUI0.window == dotChoiceUI1.window && dotChoiceUI0.representation == dotChoiceUI1.representation;
	}

	public static bool operator !=(DotChoiceUI dotChoiceUI0, DotChoiceUI dotChoiceUI1)
	{
		return dotChoiceUI0.window != dotChoiceUI1.window || dotChoiceUI0.representation != dotChoiceUI1.representation;
	}

	public override bool Equals(object obj)
	{
		if (!(obj is DotChoiceUI))
		{
			return false;
		}

		var uI = (DotChoiceUI)obj;
		return EqualityComparer<WindowUI>.Default.Equals(window, uI.window) &&
			   EqualityComparer<Text>.Default.Equals(representation, uI.representation) &&
			   IsValid == uI.IsValid;
	}

	public override int GetHashCode()
	{
		var hashCode = 413029950;
		hashCode = hashCode * -1521134295 + EqualityComparer<WindowUI>.Default.GetHashCode(window);
		hashCode = hashCode * -1521134295 + EqualityComparer<Text>.Default.GetHashCode(representation);
		hashCode = hashCode * -1521134295 + IsValid.GetHashCode();
		return hashCode;
	}
}