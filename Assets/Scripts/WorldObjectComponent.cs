﻿using UnityEngine;

public abstract class WorldObjectComponent : MonoBehaviour
{
    [ReadOnly] protected WorldObject worldObject;

    protected virtual void OnValidate()
    {
        worldObject = GetComponent<WorldObject>();
    }
}