﻿using System.Collections.Generic;
using UnityEngine;
using static ItemDialogMenuUI;

public class InspectJeffreyUI : WindowUI
{
	private InventorySlotUI selectedSlot;

	[SerializeField] private InventorySlotUI[] eqSlots;
	[SerializeField] private InventorySlotUI[] equippedSlots;
	[SerializeField] private Vector3 dropOffset;

	public void ThrowItemAway(Collectable collectable)
	{
		collectable.transform.position = GameMode.jeffrey.transform.position + GameMode.jeffrey.transform.rotation * dropOffset;                      //TODO:Move to some position in front of me
		collectable.rigidbody.gameObject.SetActive(true);
		collectable.rigidbody.isKinematic = false;
		StartCoroutine(collectable.rigidbody.SetCollisionDetectionForTime(CollisionDetectionMode.ContinuousDynamic, 3));
	}

	public void CarryItem(Collectable collectable)
	{
		collectable.rigidbody.position = InteractionReference.transform.position;
		collectable.rigidbody.isKinematic = false;
		collectable.rigidbody.gameObject.SetActive(true);
		GameMode.jeffrey.pickingObjectsUp.PickUpFromInventory(collectable.rigidbody);

		// collectable.transform.position = GameMode.jeffrey.transform.position + GameMode.jeffrey.transform.rotation * dropOffset;                      //TODO:Move to some position in front of me
		// StartCoroutine(colRb.SetCollisionDetectionForTime(CollisionDetectionMode.ContinuousDynamic, 3));
	}

	public void RemoveFromInventory(Collectable collectable_, bool carry_)
	{
		if (carry_)
			CarryItem(collectable_);
		else
			ThrowItemAway(collectable_);
	}

	private int GetIndexOf(Collectable collectable_)
	{
		for (int i = 0; i < GameMode.jeffrey.inventory.content.Length; i++)
		{
			if (GameMode.jeffrey.inventory.content[i] == collectable_)
			{
				return i;
			}
		}

		return -1;
	}

	private int GetLastOccupied()
	{
		for (int i = 0; i < GameMode.jeffrey.inventory.content.Length; i++)
		{
			if (GameMode.jeffrey.inventory.content[i] == null)
				return i;
		}

		return -1;
	}

	protected override void OnWindowOpened()
	{
		for (int i = 0; i < GameMode.jeffrey.inventory.content.Length; i++)
		{
			List<DIALOG_OPTIONS> _dialogOptions = new List<DIALOG_OPTIONS>();
			_dialogOptions.Add(DIALOG_OPTIONS.CARRY);
			_dialogOptions.Add(DIALOG_OPTIONS.DROP);

			if (GameMode.jeffrey.inventory.content[i] && GameMode.jeffrey.inventory.content[i].GetComponent<Equippable>())
				_dialogOptions.Add(DIALOG_OPTIONS.EQUIP);

			eqSlots[i].PlaceCollectable(GameMode.jeffrey.inventory.content[i], _dialogOptions.ToArray());
		}

		for (int i = 0; i < equippedSlots.Length; i++)
			equippedSlots[i].PlaceCollectable(GameMode.jeffrey.equipping.currentlyEquipped[i], DIALOG_OPTIONS.CARRY, DIALOG_OPTIONS.DROP, DIALOG_OPTIONS.UNEQUIP);
	}

	protected override void OnWindowClosed()
	{
	}
}