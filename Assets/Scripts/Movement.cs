﻿using System.Collections;
using UnityEngine;
using static GameMode;

public class Movement : MonoBehaviour
{
    public bool isGrounded;

    [HideInInspector] public bool isCrouching;

    [SerializeField] private new Rigidbody rigidbody;
    [SerializeField] private Transform meshParent;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float inputSpeed;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float jumpForce;

    private void Start()
    {
        if (!rigidbody) rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Jump();
        Rotate();
        Move();
        Crouch();
        HandleAnimator();
    }

    private void Rotate()
    {
        Vector3 delta = rotateSpeed * Input.Axs("Mouse X", GAME_STATE.GAMEPLAY) * Vector3.up * Time.deltaTime;
        Quaternion finalRot = Quaternion.Euler(transform.eulerAngles + delta);
        rigidbody.MoveRotation(finalRot);
    }

    private void Move()
    {
        Vector3 forward = Input.Axs("Vertical", GAME_STATE.GAMEPLAY) * transform.forward * inputSpeed;
        Vector3 right = Input.Axs("Horizontal", GAME_STATE.GAMEPLAY) * transform.right * inputSpeed;
        Vector3 delta = moveSpeed * Time.deltaTime * (forward + right) * (isCrouching ? 0.5f : 1);
        rigidbody.MovePosition(transform.position + delta);
    }

    private void Jump()
    {
        if (isGrounded && Input.BtnDwn("Jump", GAME_STATE.GAMEPLAY))
        {
            rigidbody.AddForce(jumpForce * Vector3.up);
            jeffrey.animator.SetTrigger("jump");
            isGrounded = false;
        }
    }

    private void Crouch()
    {
        if (Input.BtnDwn("Crouch", GAME_STATE.GAMEPLAY))
        {
            isCrouching = !isCrouching;

            StartCoroutine(Crouching(isCrouching));
        }
    }

    private void HandleAnimator()
    {
        jeffrey.animator.SetBool("crouch", isCrouching);
        jeffrey.animator.SetFloat("speed_ver", Input.Axs("Vertical", GAME_STATE.GAMEPLAY) * inputSpeed);
        jeffrey.animator.SetFloat("speed_hor", Input.Axs("Horizontal", GAME_STATE.GAMEPLAY) * inputSpeed);
    }

    private IEnumerator Crouching(bool start)
    {
        Vector3 startScale = meshParent.transform.localScale;
        Vector3 destScale = start ? new Vector3(1, 0.5f, 1) : Vector3.one;

        float time = 0.5f;
        float timer = 0;

        while (timer < time)
        {
            timer += Time.deltaTime;
            meshParent.transform.localScale = Vector3.Lerp(startScale, destScale, timer / time);

            yield return 0;
        }
    }
}