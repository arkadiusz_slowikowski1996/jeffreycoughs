﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using static GameMode;

public class TalkingUI : MonoBehaviour
{
    public static int textboxSize = 60;

    [SerializeField] private GameObject talkingBoard;
    [SerializeField] private Text talkingText;
    [SerializeField] private Transform decisionsParent;
    [SerializeField] private Button decisionPrefab;
    [SerializeField] private Color takeQuestDecisionColor;
    [SerializeField] private Color completeQuestDecisionColor;

    private Button[] currDecisions;

    private bool IsBoardOpened
    {
        get { return talkingBoard.activeInHierarchy; }
        set
        {
            talkingBoard.SetActive(value);
            currState = value ? GAME_STATE.TALKING : GAME_STATE.GAMEPLAY;
        }
    }

    private void Start()
    {
        StartCoroutine(ConstantlyCheckForThingsToSay());
    }

    private IEnumerator ConstantlyCheckForThingsToSay()
    {
        while (true)
        {
            if (Talking.linesToDisplay.Count > 0 && !IsBoardOpened)
                yield return StartCoroutine(ShowTheThingToSay_Cor());
            else
                yield return 0;
        }
    }

    private IEnumerator ShowTheThingToSay_Cor()
    {
        if (!IsBoardOpened)
        {
            IsBoardOpened = true;

            while (true)
            {
                if (Talking.linesToDisplay.Count > 0)
                    yield return StartCoroutine(Display(Talking.linesToDisplay.Dequeue()));
                else
                    break;
            }

            IsBoardOpened = false;
        }
    }

    private IEnumerator Display(Line line_)
    {
        talkingText.text = "";

        string[] _parts = GetThingSeperated(line_);

        int _progress = 0;
        bool _isThingInProgress = true;
        float _timer = 0;

        while (true)
        {
            if (_progress < _parts.Length)
            {
                talkingText.text += _parts[_progress];
                _progress++;

                _timer = 0;

                while (_timer < line_.timeBetweenParts)
                {
                    _timer += Time.deltaTime;

                    if (Input.BtnDwn("SubmitUI", GAME_STATE.TALKING) || Input.BtnDwn("Jump", GAME_STATE.TALKING))
                    {
                        while (_progress < _parts.Length)
                        {
                            talkingText.text += _parts[_progress++];
                        }
                    }

                    yield return 0;
                }
            }
            else
            {
                if (_isThingInProgress && line_.decisions != null && line_.decisions.Count > 0)
                    CreateDecisions(line_);

                _isThingInProgress = false;
            }

            if ((Input.BtnDwn("CancelUI", GAME_STATE.TALKING) || Input.BtnDwn("SubmitUI", GAME_STATE.TALKING)) && !_isThingInProgress)
            {
                if (Talking.linesToDisplay.Count == 0) Talking.FireTalkedTo(line_.author);

                break;
            }

            yield return 0;
        }

        line_.actionAfter?.Invoke();

        yield return 0;
        currDecisions = null;
    }

    private void CreateDecisions(Line line_)
    {
        float[] _poses = new float[line_.decisions.Count];
        float _rectHeight = 70;

        int j = 0;

        for (float i = (float)line_.decisions.Count / 2; i > -(float)line_.decisions.Count / 2; i--)
        {
            _poses[j] = _rectHeight * i;

            j++;
        }

        currDecisions = new Button[line_.decisions.Count];

        for (int i = 0; i < line_.decisions.Count; i++)
        {
            currDecisions[i] = Instantiate(decisionPrefab, decisionsParent);
            currDecisions[i].transform.localPosition = _poses[i] * Vector3.up;
            Text _decisionText = currDecisions[i].GetComponentInChildren<Text>();
            _decisionText.text = line_.decisions[i].questToTalkAbout.questTitle;
            _decisionText.color = line_.decisions[i].decision == Decision_Talking.DECISIONS.TAKE_A_QUEST ? takeQuestDecisionColor : completeQuestDecisionColor;

            Navigation _nav;
            if (i != 0)
            {
                _nav = currDecisions[i].navigation;
                _nav.selectOnUp = currDecisions[i - 1];
                currDecisions[i].navigation = _nav;

                _nav = currDecisions[i - 1].navigation;
                _nav.selectOnDown = currDecisions[i];
                currDecisions[i - 1].navigation = _nav;
            }

            Quest _quest = line_.decisions[i].questToTalkAbout;

            //currDecisions[i].onClick.AddListener(() => DecisionButtonListener(_quest, line_.decisions[i].decision));

            switch (line_.decisions[i].decision)
            {
                case Decision_Talking.DECISIONS.TAKE_A_QUEST:
                    currDecisions[i].onClick.AddListener(() =>
                    {
                        Questing.TakeQuest(_quest); foreach (Button b in currDecisions)
                            Destroy(b.gameObject);
                        currDecisions = null;
                    });
                    //Questing.TakeQuest(_quest);
                    break;
                case Decision_Talking.DECISIONS.COLLECT_A_QUEST:
                    currDecisions[i].onClick.AddListener(() =>
                    {
                        Questing.CompleteQuest(_quest); foreach (Button b in currDecisions)
                            Destroy(b.gameObject);
                        currDecisions = null;
                    });
                    //Questing.CompleteQuest(_quest);
                    break;
            }
        }

        currDecisions[0].Select();
    }

    private void DecisionButtonListener(Quest quest_, Decision_Talking.DECISIONS decision_)
    {
        switch (decision_)
        {
            case Decision_Talking.DECISIONS.TAKE_A_QUEST:
                Questing.TakeQuest(quest_);
                break;
            case Decision_Talking.DECISIONS.COLLECT_A_QUEST:
                Questing.CompleteQuest(quest_);
                break;
        }

        foreach (Button b in currDecisions)
            Destroy(b.gameObject);
        currDecisions = null;
    }

    private string[] GetThingSeperated(Line thingToSay_)
    {
        string[] _returnValue = null;

        switch (thingToSay_.displayMode)
        {
            case Line.DISPLAY_MODE.LETTERS:
                _returnValue = new string[thingToSay_.text.Length];
                for (int i = 0; i < _returnValue.Length; i++)
                    _returnValue[i] = thingToSay_.text[i].ToString();
                break;
            case Line.DISPLAY_MODE.WORDS:
                _returnValue = thingToSay_.text.Split(' ');
                for (int i = 0; i < _returnValue.Length; i++)
                    if (i != _returnValue.Length - 1) _returnValue[i] += " ";
                break;
            case Line.DISPLAY_MODE.SENTENCE:
                _returnValue = thingToSay_.text.Split('.');
                for (int i = 0; i < _returnValue.Length; i++)
                    if (i != _returnValue.Length - 1) _returnValue[i] += ".";
                break;
            case Line.DISPLAY_MODE.WHOLE:
                _returnValue = new string[1];
                _returnValue[0] = thingToSay_.text;
                break;
        }

        return _returnValue;
    }
}