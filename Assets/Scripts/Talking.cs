﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Talking : MonoBehaviour
{
	public delegate void TalkingTo(string target_);
	public static event TalkingTo TalkedTo;

	private static Talking ciop;

	public static Queue<Line> linesToDisplay = new Queue<Line>();

	private void Awake()
	{
		ciop = this;
	}

	private void Update()
	{
		if (Input.BtnDwn("TalkTo", GameMode.GAME_STATE.GAMEPLAY))
		{
			Debug.Log("Talk to");
			WorldObject worldObject = Raycasting.hitWorldObject;
			if (Raycasting.IsHitInRange && worldObject)
			{
				Talker talker = worldObject.talker;

				if (talker)
				{
					talker.TalkTo();
				}
			}
		}
	}

	public static void Say(Line line_)
	{
		ciop.AddAThingToSay(line_);
	}

	public static void Say(Line[] lines_)
	{
		for (int i = 0; i < lines_.Length; i++)
		{
			ciop.AddAThingToSay(lines_[i]);
		}
	}

	public static void FireTalkedTo(string target_)
	{
		TalkedTo?.Invoke(target_);
	}

	private Line AddAThingToSay(Line thingToBeAdded_)
	{
		linesToDisplay.Enqueue(thingToBeAdded_);

		return thingToBeAdded_;
	}

	private Line[] AddThingsToSay(Line[] thingsToBeAdded_)
	{
		foreach (Line thingToSay in thingsToBeAdded_)
			linesToDisplay.Enqueue(thingToSay);

		return thingsToBeAdded_;
	}
}

[Serializable]
public class Line
{
	public enum DISPLAY_MODE { LETTERS, WORDS, SENTENCE, WHOLE }
	public DISPLAY_MODE displayMode = DISPLAY_MODE.LETTERS;

	public string text = "";
	public float timeBetweenParts = 0.5f;
	//public int priority = 1;
	public Action actionAfter = null;
	public List<Decision_Talking> decisions = new List<Decision_Talking>();
	public TalkingSounds.SOUNDS soundToMake;

	[HideInInspector] public string author;

	private AudioClip sound;
	public AudioClip Sound
	{
		get
		{
			if (sound == null)
				sound = TalkingSounds.GetSound(soundToMake);
			return sound;
		}
	}

	public static Line Copy(Line original_, string authorName_)
	{
		original_.author = authorName_;

		return new Line()
		{
			displayMode = original_.displayMode,
			text = original_.text,
			timeBetweenParts = original_.timeBetweenParts,
			author = original_.author,
			actionAfter = original_.actionAfter,
			decisions = new List<Decision_Talking>(original_.decisions),
			soundToMake = original_.soundToMake,
			sound = original_.sound
		};
	}

	public static Line[] Copy(Line[] original_, string authorName_)
	{
		Line[] _returnValue = new Line[original_.Length];

		for (int i = 0; i < original_.Length; i++)
			_returnValue[i] = Copy(original_[i], authorName_);

		return _returnValue;
	}

	public override string ToString()
	{
		return text;
	}
}

[Serializable]
public class Decision_Talking
{
	public enum DECISIONS { TAKE_A_QUEST, COLLECT_A_QUEST }
	public DECISIONS decision;
	public Quest questToTalkAbout;

	public static Decision_Talking CreateDecitionAboutQuest(Quest quest_, DECISIONS decision_)
	{
		Decision_Talking _returnValue = new Decision_Talking();
		_returnValue.questToTalkAbout = quest_;
		_returnValue.decision = decision_;

		return _returnValue;
	}
}