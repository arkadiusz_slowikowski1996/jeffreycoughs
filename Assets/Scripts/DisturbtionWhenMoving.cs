﻿using System.Collections.Generic;
using UnityEngine;

public class DisturbtionWhenMoving : MonoBehaviour
{
    [ReadOnly] public float extra;

    [SerializeField] private new Rigidbody rigidbody;
    [SerializeField] private float standingValue;
    [SerializeField] private float crouchingValue;
    [SerializeField] private float extraMultiplier;
    [SerializeField] private float maxValue;
    [SerializeField] private float maxVelocityMagnitude;

    private Vector3 prevPos;

    public float Disturbtion
    {
        get
        {
            float _distance = Mathf.Abs(Vector3.Distance(transform.position, prevPos));
            float _valueToReturn = (_distance / maxVelocityMagnitude) * maxValue;

            return _valueToReturn + extra * extraMultiplier + (!GameMode.jeffrey.movement.isCrouching ? standingValue : crouchingValue);
        }
    }

    private void FixedUpdate()
    {
        prevPos = transform.position;
    }

    public float GetDisturbionWithExtra(float extra_)
    {
        return Mathf.Clamp(Disturbtion + extra_ * maxValue, 0, maxValue);
    }

    public float GetDisturbionWithExtraUnclamped(float extra_)
    {
        return Disturbtion + extra_ * maxValue;
    }
}