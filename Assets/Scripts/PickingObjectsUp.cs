﻿using System.Collections;
using UnityEngine;

public class PickingObjectsUp : MonoBehaviour
{
	public InteractionReference liftingJoint;

	[SerializeField] private float timeToHoldButtonDuring;

	private float obl_originalDrag;
	private float obl_originalAngularDrag;
	private CollisionDetectionMode obl_originalCollisionDetectionMode;

	private void Start()
	{
		StartCoroutine(CountTimeWhenPressed());
	}

	private IEnumerator CountTimeWhenPressed()
	{
		while (true)
		{
			if (Input.BtnDwn("PickUp", GameMode.GAME_STATE.GAMEPLAY))
			{
				float _timer = 0;
				while (Input.Btn("PickUp", GameMode.GAME_STATE.GAMEPLAY))
				{
					_timer += Time.deltaTime;

					if (_timer >= timeToHoldButtonDuring)
						break;

					yield return 0;
				}

				if (_timer >= timeToHoldButtonDuring)
				{
					if (!liftingJoint.ConnectedBody)
					{
						WorldObject worldObject = Raycasting.hitWorldObject;

						if (worldObject)
						{
							PickUpable pickUpable = worldObject.pickUpable;

							if (pickUpable && Raycasting.IsHitInRange)
							{
								PickUp(pickUpable.rigidbody, Raycasting.currentHit.point);
							}
						}
					}
					else
					{
						liftingJoint.SetConnectedBody(null, Vector3.zero);
					}
				}
			}

			yield return 0;
		}
	}

	public void PickUp(Rigidbody rigidbody, Vector3 point)
	{
		if (!liftingJoint.ConnectedBody)
		{
			liftingJoint.SetConnectedBody(rigidbody, point);
		}
		else
		{
			liftingJoint.SetConnectedBody(null, Vector3.zero);
		}
	}

	public void PickUpFromInventory(Rigidbody rigidbody)
	{
		StartCoroutine(PickUpFromInventory_Cor(rigidbody));
	}

	private IEnumerator PickUpFromInventory_Cor(Rigidbody toPickup)
	{
		toPickup.isKinematic = true;

		Vector3 pos = InteractionReference.transform.TransformPoint(0.25f * Vector3.forward);
		toPickup.position = pos;
		toPickup.velocity = Vector3.zero;
		toPickup.gameObject.SetActive(true);

		yield return 0;
		toPickup.velocity = Vector3.zero;
		GameMode.jeffrey.pickingObjectsUp.PickUp(toPickup, pos);
		yield return 0;
		toPickup.isKinematic = false;

		toPickup.velocity = Vector3.zero;
	}
}