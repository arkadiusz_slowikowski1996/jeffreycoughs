﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagecoachWC : MonoBehaviour
{
	[SerializeField] private WheelCollider[] frontWheels;
	[SerializeField] private WheelCollider[] allWheels;
	[SerializeField] private float steerSpeed;
	[SerializeField] private float torqueSpeed;

	private void Update()
	{
		for (int i = 0; i < frontWheels.Length; i++)
		{
			frontWheels[i].steerAngle += steerSpeed * Input.Axs("Horizontal") * Time.deltaTime;
		}

		for (int i = 0; i < allWheels.Length; i++)
		{
			allWheels[i].motorTorque += torqueSpeed * Input.Axs("Vertical") * Time.deltaTime;
		}
	}
}